+++
title = "CCP - Summary"
weight = 10
draft = false
+++

Types of Cloud Computing

-   Infrastructure as a Service (IaaS) =\> Amazon EC2

    -   Provide building blocks for cloud IT
    -   Provides networking, computers, data storage space
    -   Highest level of flexibility
    -   Easy parallel with traditional on-premises IT

-   Platform as a Service (PaaS) =\> Elastic Beanstalk

    -   Removes the need for your organization to manage the underlying
        infrastructure
    -   Focus on the deployment and management of your applications

-   Software as a Service (SaaS)

    -   Completed product that is run and managed by the service
        provider

AWS Global Infrastructure

-   AWS Regions

    -   AWS has regions all around the world
    -   Names can be us-east-I, eu-west-3...
    -   A region is a cluster of data centers
    -   Most AWS services are region-scoped

-   AWS Availability Zones

    -   each region has many availability zones(usually 3, min is 3, max
        is 6)
    -   each AZ is one or more discrete data centers with redundant
        powr, networking, and connectivity
    -   They're separate from each other, so that they're isolated from
        disasters
    -   They're connected with high bandwidth, ultra-low latency
        networking

-   AWS Data Centers

-   AWS Edge Locations / Points of Presence

    -   Amazon has 216 points of presence in 84cities across 42
        countries
    -   Content is delivered to end users with lower latency
    -   Amazon CloudFront가 더 빠른 콘텐츠 전송을 위해 고객과 가까운
        위치에 콘텐츠 사본을 캐시하는데 사용하는 site

Tour of the AWS Console

-   AWS has Global Services:

    -   Identity and Access Management (IAM)
    -   Route 53 (DNS service)
    -   CloudFront (Content Delivery Network)
    -   WAF (Web Application Firewall)

-   Most AWS services are region-scoped

    -   Amazon EC2 (IaaS)
    -   Elastic Beanstalk (Platform as a Service)
    -   Lamda( Function as a Service)
    -   Rekognition (Software as Service)\

Shared Responsibility Model

-   customer =\> responsibility for the security IN the cloud
-   AWS =\> responsibility for the security OF the cloud

IAM: Users & Group

-   IAM = Identity and Access Management, Global service
-   Root account created by default, shouldn't be used or shared
-   Users are people within your organization, and can be grouped
-   Groups only contain users, not other groups
-   Users don't have to belong to a group, and user can belong to
    multiple groups

IAM: Permissions

-   Users or Groups can be assigned JSON documents called policies
-   Theses policies define the permissions of the users

MFA (Multi Factor Authentication)

-   Virtual MFA device: Google Authenticator, Authy
-   Universal 2^nd^ Factor (U2F) security Key: YubiKey
-   Hardware Key Fob MFA Device: Gemalto
-   Hardware Key Fob MFA Device for AWS GovCloud (US): SurePassID

How can users access AWS

-   three options

    -   AWS Management Console(password + MFA)
    -   AWS Command Line Interface (CLI): access keys
    -   AWS Software Developer Kit (SDK): for code, access keys

-   Access keys are generated through the AWS console

-   Access Key ID = username

-   Secret Access Key = password

Amazon EC2 = Elastic Compute Cloud = Infrastructure as as Service

-   EC2 Sizing & configuration options

    -   Operating System(OS): Linux, Windows, MacOS

    -   How much compute power & cores (CPU)

    -   How much random-access memory (RAM)

    -   How much storage space:

        -   network-attached(EBS & EFS)
        -   hardware (EC2 Instance Store)

    -   Network card: speed of the card, Public IP address

    -   Firewall rules: security group

    -   Bootstrap script(configure at first launch): EC2 User Data

-   EC2 User Data

    -   it is possible to bootstrap our instances using an EC2 User Data
        script

    -   bootstrapping means launching commands when a machine starts

    -   That script is only run once at the instance first start

    -   EC2 user data is used to automate boot tasks such as

        -   Installing updates
        -   Installing software
        -   Downloading common files from the internet

    -   The EC2 User Data Script runs with the root user

EC2 Instance Types

-   General Purpose
-   Compute Optimized: Great for compute-intensive tasks that require
    high performance processors
-   Memory Optimized: Fast performance for workloads that process large
    data sets in memory
-   Storage Optimized

Security Group

-   Security Groups = fundamental of network security in AWS

-   They control how traffic is allowed into our out of our EC2
    Instances

-   Security groups only contain allow rules

-   Security groups rules can reference by IP or by security group

-   Security groups are acting as a "Firewall" on EC2 instances

    -   Access to ports
    -   Authorized IP ranges: IPv4 and IPv6
    -   Control of inbound network (from other to the instance)
    -   Control of outbound network(from the instance to other)

-   can be attached to multiple instances

-   Locked down to a region / VPC combination

-   Does live "outside" the EC2: if traffic is blocked the EC2 instance
    won't see it

-   It's good to maintain one separate security group for SSH access

-   if your application is not accessible(time out), then it's a
    security group issue

-   if your application gives a "connection refused" error, then it's an
    application error or it's not launched

-   All inbound traffic is blocked by default

-   All outbound traffic is authorized by default

Classic Ports

-   22 = SSH (Secure Shell): log into a Linux instance
-   21 = FTP (File Transfer Protocol) : upload files into a file share
-   22 = SFTP (Secure File Transfer Protocol) : upload files using SSH
-   80 = HTTP : access unsecured websites
-   443 = HTTPS : acess secured websites
-   3389 = RDP (Remote Desktop Protocol) : log into a Windows instance

EC2 Instances Purchasing Options

-   On-Demand Instances -- short workload, predictable pricing, pay by
    second

    -   Has the highest cost but no upfront payment
    -   No long-term commitment
    -   recommended for short-term and un-interrupted workloads, where
        you can't predict how the application will behave

-   Reserved (1 or 3 years)

    -   Reserved Instances -- long workloads
    -   Convertible Reserved Instances -- long workloads with flexible
        instances

-   Savings Plans (1 or 3 years) --commitment to an amount of usage,
    long workload

-   Spot Instances -- short workloads, cheap, can lose instances (less
    reliable)

    -   Most cost-efficient instances in AWS
    -   not suitable for critical jobs or databases

-   Dedicated Hosts -- book an entire physical server, control instance
    placement

    -   A physical server with EC2 instance capacity fully dedicated to
        your use

    -   Allows you address compliance requirements and use your existing
        server- bound software licenses (per-socket, per-core, pe---VM
        software licenses)

    -   Purchasing Options:

        -   On-demand -- pay per second for active Dedicated Host
        -   Reserved - 1 or 3 years (No Upfront, Partial Upfront, All
            Upfront)

    -   The most expensive option

    -   Useful for software that have complicated licensing model (BYOL
        -- Bring Your Own License) Or for companies that have strong
        regulatory or compliance needs

-   Dedicated Instances -- no other customers will share your hardware

    -   Instances run on hardware that's dedicated to you
    -   May share hardware with other instances in same account
    -   No control over instance placement (can move hardware after Stop
        / Start)

-   Capacity Reservations -- reserve capacity in a specific AZ for any
    duration

    -   Reserve On-Demand instances capacity in a specific AZ for any
        duration
    -   You always have access to EC2 capacity when you need it
    -   No time commitment (create/cancel anytime), no billing discounts
    -   Combine with Regional Reserved Instances and Savings Plans to
        benefit from billing discounts
    -   You're charged at On-Demand rate whether you run instances or
        not
    -   Suitable for short-term, uninterrupted workloads that needs to
        be in a specific AZ

Which purchasing option is right for me?

-   On demand: coming and staying in resort whenever we like, we pay the
    full price
-   Reserved: like planning ahead and if we plan to stay for a long
    time, we may get a good discount.
-   Savings Plans: pay a certain amount per hour for certain period and
    stay in any room type (e.g., King, Suite, Sea View, ...)
-   Spot instances: the hotel allows people to bid for the empty rooms
    and the highest bidder keeps the rooms. You can get kicked out at
    any time
-   Dedicated Hosts: We book an entire building of the resort
-   Capacity Reservations: you book a room for a period with full price
    even you don't stay in it

EBS Volume = Elastice Block Store Volume

-   EBS is a network drive you can attach to your instances while they
    run

-   It can be detached from an EC2 instance and attached to another one
    quickly

-   It allows your instances to persist data, even after their
    termination

-   They can only be mounted to one instance at a time (at the CCP
    level)

-   They are bound to a specific availability zone

    -   An EBS volume in us-east-1a cannot be attached to us-east-1b
    -   to move a volume across, you first need to snapshot it

-   Think of them as a "network USB stick"

-   Controls the EBS behavior when an EC2 instance terminates

    -   by default, the root EBS volume is deleted (attribute enabled)
    -   by default, any other attached EBS volume is not deleted
        (attribute disabled)

-   EBS snapshots

    -   make a backup(snapshot) of your EBS volume at a point in time
    -   Not necessary to detach volume to do snapshot, but recommended
    -   Can copy snapshots across AZ or Region

AMI = Amazon Machine Image

-   AMI are a customization of an EC2 instance

    -   You add your own software, configuration, operating system,
        monitoring..
    -   Fast boot / configuration time because all your software is
        pre-packaged

-   AMI are built for a specific region(and can be copied across
    regions)

-   You can launch EC2 instances from

    -   A public AMI: AWS provided
    -   Your own AMI
    -   AWS Marketplace AMI: an AMI someone else made

EC2 Image Builder

-   Used to automate the create of Virtual Machines or container images
-   Automate the creation, maintain, validate and test EC2 AMIs
-   can be run on schedule
-   Free service(only play for the underlying resources)

EC2 Instance Store

-   EBS volumes are network drives with good but "limited" performance
-   if you need a high-performance hardware disk, use EC2 Instance Store
-   Better I/O performance
-   EC2 Instance Store lose their storage if they're stopped(ephemeral)
-   Good for buffer/cache/scratch data/temporary content
-   Risk of data loss if hardware fails
-   Backups and Replication are your responsibility

EFS = Elastic File System

-   Managed NFS(Network File System) that can be mounted on 100s of EC2
-   EFS works with Linux EC2 instances in multi-AZ
-   ![](Pictures/10000001000003F900000242E911645F06F7BA24.png){width="2.7134in"
    height="1.5425in"}High available, scalable, expensive, pay per use,
    no capacity planning

EFS Infrequent Access (EFS-IA)

-   Storage Class that is cost-optimized for files not accessed every
    day
-   Up to 92% lower cost compared to EFS standard
-   EFS will automatically move your files to EFS-IA based on the last
    time they were accessed
-   Enable EFS-IA with a Lifecycle Policy

![](Pictures/100000010000017400000205DB8511D06D15B647.png){width="2.1339in"
height="2.9661in"}

Shared Responsibility Model for EC2 Storage

-   AWS

    -   Replication for data for EBS volumes & EFS drives
    -   Replacing faulty hardware • Ensuring their employees cannot
        access your data

-   Customer

    -   Setting up backup / snapshot procedures
    -   Setting up data encryption
    -   Responsibility of any data on the drives
    -   Understanding the risk of using EC2 Instance Store

Amazon FSx

-   Launch 3^rd^ party high-performance file systems on AWS
-   Fully managed service
-   FSx for Windows: Network File System for Windows servers
-   FSx for Lustre: High Performance Computing Linux file system

Scalability

-   Scalability means that an application / system can handle greater
    loads by adapting

-   There are two kinds of scalability

    -   Vertical Scalability: it means increasing the size of the
        instance

        -   ex) your application runs on a t2.micro =\> t2.large

        -   It's common for non distributed systems, such as a database

        -   There's usually a limit to how much you can vertically
            scale(hardware limit)

    -   Horizontal Scalability = elasticity

        -   it means increasing the number of instances/systems for your
            application
        -   Horizontal scaling implies distributed systems
        -   This is very common for web applications/modern applications

-   Scalability is linked but different to High Availability

High Availability

-   High Availability usually goes hand in hand with horizontal scaling
-   high availability means running your application/system in at least
    2 Availability Zones
-   The goal of high availability is to survive a data center loss
    (disaster)

High Availability & Scalability For EC2

-   Vertical Scaling: Increase instance size (= scale up / down)

    -   From: t2.nano - 0.5G of RAM, 1 vCPU
    -   To: u-12tb1.metal -- 12.3 TB of RAM, 448 vCPUs

-   Horizontal Scaling: Increase number of instances (= scale out / in)

    -   Auto Scaling Group
    -   Load Balancer

-   High Availability: Run instances for the same application across
    multi AZ

    -   Auto Scaling Group multi AZ
    -   Load Balancer multi AZ

Scalability vs Elasticity (vs Agility) •

-   Scalability: ability to accommodate a larger load by making the
    hardware stronger (scale up), or by adding nodes (scale out)
-   Elasticity: once a system is scalable, elasticity means that there
    will be some "auto-scaling" so that the system can scale based on
    the load. This is "cloud-friendly": pay-per-use, match demand,
    optimize costs
-   Agility: (not related to scalability - distractor) new IT resources
    are only a click away, which means that you reduce the time to make
    those resources available to your developers from weeks to just
    minutes.

Load Balancing

-   Load Balancers are servers that forward internet traffic to multiple
    servers(EC2 instances) downstream.

Elastic Load Balancer

-   An ELB (Elastic Load Balancer) is a managed load balancer

    -   AWS guarantees that it will be working
    -   AWS takes care of upgrades, maintenance, high availability
    -   AWS provides only a few configuration knobs

-   It costs less to setup your own load balancer but it will be a lot
    more effort on your end (maintenance, integrations)

-   3 kinds of load balancers offered by AWS:

    -   Application Load Balancer (HTTP / HTTPS only) -- Layer 7
    -   Network Load Balancer (ultra-high performance, allows for TCP)
        -- Layer 4
    -   Gateway Load Balancer -- Layer 3

![](Pictures/10000001000004B200000259337BF4F56DB70CE7.png){width="6.9252in"
height="3.4618in"}

Auto Scaling Group (ASG)

-   The goal of an Auto Scaling Group (ASG) is to:

    -   Scale out (add EC2 instances) to match an increased load
    -   Scale in (remove EC2 instances) to match a decreased load
    -   Ensure we have a minimum and a maximum number of machines
        running
    -   Automatically register new instances to a load balancer
    -   Replace unhealthy instances

-   Scaling Strategies

    -   Manual Scaling: Update the size of an ASG manually

    -   Dynamic Scaling: Respond to changing demand

        -   Simple / Step Scaling

            -   When a CloudWatch alarm is triggered (example CPU \>
                70%), then add 2 units
            -   When a CloudWatch alarm is triggered (example CPU \<
                30%), then remove 1

        -   Target Tracking Scaling

            -   Example: I want the average ASG CPU to stay at around
                40%

        -   Scheduled Scaling

            -   Anticipate a scaling based on known usage patterns
            -   Example: increase the min. capacity to 10 at 5 pm on
                Fridays

        -   Predictive Scaling

            -   Uses Machine Learning to predict future traffic ahead of
                time
            -   Automatically provisions the right number of EC2
                instances in advance
            -   Useful when your load has predictable time - based
                patterns

Amazon S3 - Buckets

-   Amazon S3 allows people to store objects (files) in "buckets"
    (directories)
-   Buckets must have a globally unique name (across all regions all
    accounts)
-   Buckets are defined at the region level
-   S3 looks like a global service but buckets are created in a region
-   Naming convention

Amazon S3 - Objects

-   Objects (files) have a Key

-   The key is the FULL path:

    -   s3://my-bucket/my\_file.txt
    -   s3://my-bucket/my\_folder1/another\_folder/my\_file.txt

-   The key is composed of prefix + object name

    -   s3://my-bucket/my\_folder1/another\_folder/my\_file.txt

-   There's no concept of "directories" within buckets (although the UI
    will trick you to think otherwise)

-   Just keys with very long names that contain slashes ("/")

Amazon S3 -- Security

-   User-Based

    -   IAM Policies -- which API calls should be allowed for a specific
        user from IAM

-   Resource-Based

    -   Bucket Policies -- bucket wide rules from the S3 console -
        allows cross account •
    -   Object Access Control List (ACL) -- finer grain (can be
        disabled)
    -   Bucket Access Control List (ACL) -- less common (can be
        disabled)

-   Note: an IAM principal can access an S3 object if

    -   The user IAM permissions ALLOW it OR the resource policy ALLOWS
        it
    -   AND there's no explicit DENY

-   Encryption: encrypt objects in Amazon S3 using encryption keys

Amazon S3 -- Static Website Hosting

-   S3 can host static websites and have them accessible on the Internet

-   The website URL will be (depending on the region)

    -   http://bucket-name.s3-website-aws-region.amazonaws.com

-   If you get a 403 Forbidden error, make sure the bucket policy allows
    public reads!

Amazon S3 -Versioning

-   You can version your files in Amazon S3

-   It is enabled at the bucket level

-   Same key overwrite will change the "version": 1, 2, 3....

-   It is best practice to version your buckets

    -   Protect against unintended deletes (ability to restore a
        version)
    -   Easy roll back to previous version

-   Notes:

    -   Any file that is not versioned prior to enabling versioning will
        have version "null"
    -   Suspending versioning does not delete the previous versions

Amazon S3 -- Replication (CRR & SRR)

-   Must enable Versioning in source and destination buckets

-   Cross-Region Replication (CRR)

-   Same-Region Replication (SRR)

-   Buckets can be in different AWS accounts

-   Copying is asynchronous

-   Must give proper IAM permissions to S3

-   Use cases:

    -   CRR -- compliance, lower latency access, replication across
        accounts
    -   SRR -- log aggregation, live replication between production and
        test accounts

S3 Storage Classes

-   Amazon S3 Standard - General Purpose

    -   Use Cases: Big Data analytics, mobile & gaming applications,
        content distribution...

-   Amazon S3 Infrequent Access:

    -   For data that is less frequently accessed, but requires rapid
        access when needed

    -   Amazon S3 Standard-Infrequent Access (IA)

        -   Use cases: Disaster Recovery, backups

    -   Amazon S3 One Zone-Infrequent Access

        -   Use Cases: Storing secondary backup copies of on-premise
            data, or data you can recreate

-   Amazon S3 Glacier:

    -   Low-cost object storage meant for archiving / backup

    -   Pricing: price for storage + object retrieval cost

    -   Amazon S3 Glacier Instant Retrieval

        -   great for data accessed once a quarter • Minimum storage
            duration of 90 days

    -   Amazon S3 Glacier Flexible Retrieval

        -   Expedited (1 to 5 minutes), Standard (3 to 5 hours), Bulk (5
            to 12 hours) -- fre
        -   Minimum storage duration of 90 days

    -   Amazon S3 Glacier Deep Archive : for long term storage

        -   Standard (12 hours), Bulk (48 hours) • Minimum storage
            duration of 180 days

-   Amazon S3 Intelligent Tiering

    -   Small monthly monitoring and auto-tiering fee
    -   Moves objects automatically between Access Tiers based on usage
    -   There are no retrieval charges in S3 Intelligent-Tiering

-   Can move between classes manually or using S3 Lifecycle
    configurations

S![](Pictures/1000000100000406000001E267D4ED6611815F23.png){width="3.9965in"
height="1.7181in"}3 Encryption

AWS Snow Family (for Data Migration)

-   offline devices to perform data migrations If it takes more than a
    week to transfer over the network, use Snowball devices!

```{=html}
<!-- -->
```
-   Highly-secure, portable devices to collect and process data at the
    edge, and migrate data into and out of AWS

-   Data migration:

    -   Snowcone, Snowball Edge, Snowmobile

-   Edge computing

    -   Snowcone, Snowball Edge

-   Snowball Edge

    -   Physical data transport solution: move TBs or PBs of data in or
        out of AWS

    -   Pay per data transfer job

    -   Use cases: large data cloud migrations, DC decommission,
        disaster recovery

-   Snowcone

    -   Small, light, portable computing, anywhere, rugged & secure,
        withstands harsh environments

    -   Device used for edge computing, storage, and data transfer

    -   8 TBs of usable storage

    -   Use Snowcone where Snowball does not fit (space -constrained
        environment)

    -   Must provide your own battery / cables

    -   Can be sent back to AWS offline, or connect it to internet and
        use AWS DataSync to send data

-   Snowmobile

    -   Transfer exabytes of data (1 EB = 1,000 PB = 1,000,000 TBs)
    -   Each Snowmobile has 100 PB of capacity (use multiple in
        parallel)
    -   High security: temperature controlled, GPS, 24/7 video
        surveillance
    -   Better than Snowball if you transfer more than 10 PB

![](Pictures/1000000100000453000001AF93B6FF5B4972923B.png){width="6.9252in"
height="2.6957in"}

What is Edge Computing

-   Process data while it's being created on an edge location

    -   A truck on the road, a ship on the sea, a mining station
        underground...

-   These locations may have

    -   Limited / no internet access
    -   Limited / no easy access to computing power

-   We setup a Snowball Edge / Snowcone device to do edge computing

-   Use cases of Edge Computing:

    -   Preprocess data
    -   Machine learning at the edge
    -   Transcoding media streams

-   Eventually (if need be) we can ship back the device to AWS (for
    transferring data for example)

AWS OpsHub (for Snow Family)

-   Historically, to use Snow Family devices, you needed a CLI (Command
    Line Interface tool

-   Today, you can use AWS OpsHub (a software you install on your
    computer / laptop) to manage your Snow Family Device

    -   Unlocking and configuring single or clustered devices
    -   Transferring files
    -   Launching and managing instances running on Snow Family Devices
    -   Monitor device metrics (storage capacity, active instances on
        your device)
    -   Launch compatible AWS services on your devices (ex: Amazon EC2
        instances, AWS DataSync, Network File System (NFS))

AWS Storage Gateway

-   S3 is a proprietary storage technology (unlike EFS / NFS), so how do
    you expose the S3 data on-premise? =\> Storage Gateway

```{=html}
<!-- -->
```
-   Bridge between on-premise data and cloud data in S3
-   Hybrid storage service to allow on- premises to seamlessly use the
    AWS Cloud
-   Use cases: disaster recovery, backup & restore, tiered storage

Amazon S3 -- Summary

-   Buckets vs Objects: global unique name, tied to a region
-   S3 security: IAM policy, S3 Bucket Policy (public access), S3
    Encryption
-   S3 Websites: host a static website on Amazon S3
-   S3 Versioning: multiple versions for files, prevent accidental
    deletes • S3 Replication: same-region or cross-region, must enable
    versioning
-   S3 Storage Classes: Standard, IA, 1Z-IA, Intelligent, Glacier
    (Instant, Flexible, Deep)
-   Snow Family: import data onto S3 through a physical device, edge
    computing
-   OpsHub: desktop application to manage Snow Family devices
-   Storage Gateway: hybrid solution to extend on-premises storage to S3

NoSQL Databases

-   NoSQL = non-SQL = non relational databases

-   NoSQL databases are purpose built for specific data models and have
    flexible schemas for building modern applications.

-   Benefits:

    -   Flexibility: easy to evolve data model
    -   Scalability: designed to scale-out by using distributed clusters
    -   High-performance: optimized for a specific data model
    -   Highly functional: types optimized for the data model

-   Examples: Key-value, document, graph, in-memory, search databases

-   JSON = JavaScript Object Notation

    -   common form of data that fits into a NoSQL model

AWS RDS(Relational Database Service)

-   It's a managed DB service for DB use SQL as a query language.

-   It allows you to create databases in the cloud that are managed by
    AWS

    -   Postgres, MySQL, MariaDB, Oracle, Microsoft SQL Server, Aurora
        (AWS Proprietary database)

-   Advantage over using RDS vs deploying DB on EC2.

    -   Automated provisioning, OS patching
    -   Continuous backups and restore to specific timestamp (Point in
        Time Restore)!
    -   Monitoring dashboards
    -   Read replicas for improved read performance
    -   Multi AZ setup for DR (Disaster Recovery)
    -   Maintenance windows for upgrades
    -   Scaling capability (vertical and horizontal)
    -   Storage backed by EBS (gp2 or io1)

Amazon Aurora

-   Aurora is a proprietary technology from AWS (not open sourced)
-   PostgreSQL and MySQL are both supported as Aurora DB
-   Aurora is "AWS cloud optimized" and claims 5x performance
    improvement over MySQL on RDS, over 3x the performance of Postgres
    on RDS
-   Aurora storage automatically grows in increments of 10GB, up to 64
    TB.
-   Aurora costs more than RDS (20% more) -- but is more efficient / Not
    in the free tier

Amazon ElastiCache

-   자주 사용되는 요청의 읽기 시간을 향상하기위해 데이터베이스 위에 캐싱
    계층을 추가하는 서비스로, 2가지 데이터 저장소 Redis, Memcached를
    지원

```{=html}
<!-- -->
```
-   The same way RDS is to get managed Relational Databases...
-   ElastiCache is to get managed Redis or Memcached
-   Caches are in-memory databases with high performance, low latency
-   Helps reduce load off databases for read intensive workloads
-   AWS takes care of OS maintenance / patching, optimizations, setup,
    configuration, monitoring, failure recovery and backups

DynamoDB

-   Fully Managed Highly available with replication across 3 AZ
-   NoSQL database - not a relational database
-   Scales to massive workloads, distributed "serverless" database
-   Fast and consistent in performance
-   Single-digit millisecond latency -- low latency retrieval
-   Integrated with IAM for security, authorization and administration
-   Low cost and auto scaling capabilities
-   Standard & Infrequent Access (IA) Table Class

DynamoDB Accelerator -- DAX

-   Fully Managed in-memory cache for DynamoDB
-   Difference with ElastiCache at the CCP level: DAX is only used for
    and is integrated with DynamoDB, while ElastiCache can be used for
    other databases

Amazon Redshift

-   빅데이터 분석에 사용할 수 있는 데이터 웨어하우징 서비스
-   Redshift is based on PostgreSQL, but it's not used for OLTP
-   It's OLAP -- online analytical processing (analytics and data
    warehousing)
-   Load data once every hour, not every second
-   10x better performance than other data warehouses, scale to PBs of
    data
-   Massively Parallel Query Execution (MPP), highly available
-   Pay as you go based on the instances provisioned
-   Has a SQL interface for performing the queries
-   BI tools such as AWS Quicksight or Tableau integrate with it

Amazon EMR(Elastic MapReduce)

-   EMR helps creating Hadoop clusters (Big Data) to analyze and process
    vast amount of data
-   The clusters can be made of hundreds of EC2 instances
-   Also supports Apache Spark, HBase, Presto, Flink...
-   EMR takes care of all the provisioning and configuration
-   Auto-scaling and integrated with Spot instances
-   Use cases: data processing, machine learning, web indexing, big
    data...

Amazon Athena

-   표준 SQL을 사용해 S3에 저장된 데이터를 간편하게 분석할 수 있는
    대화식 쿼리서비스

```{=html}
<!-- -->
```
-   Serverless query service to analyze data stored in Amazon S3
-   Uses standard SQL language to query the files
-   Supports CSV, JSON, ORC, Avro, and Parquet (built on Presto)
-   Use compressed or columnar data for cost-savings (less scan)
-   Use cases: Business intelligence / analytics / reporting, analyze &
    query VPC Flow Logs, ELB Logs, CloudTrail trails, etc\...
-   Exam Tip: analyze data in S3 using serverless SQL, use Athena

Amazon QuickSight

-   Serverless machine learning-powered business intelligence service to
    create interactive dashboards

-   Fast, automatically scalable, embeddable, with per-session pricing

-   Use cases:

    -   Business analytics, Building visualizations, Perform ad-hoc
        analysis , Get business insights using data,

-   Integrated with RDS, Aurora, Athena, Redshift, S3...

DocumentDB

-   Aurora is an "AWS-implementation" of PostgreSQL / MySQL ...
-   DocumentDB is the same for MongoDB (which is a NoSQL database)
-   MongoDB is used to store, query, and index JSON data
-   Similar "deployment concepts" as Aurora
-   Fully Managed, highly available with replication across 3 AZ
-   DocumentDB storage automatically grows in increments of 10GB, up to
    64 TB.
-   Automatically scales to workloads with millions of requests per
    seconds

Amazon Neptune

-   Fully managed graph database

-   A popular graph dataset would be a social network

    -   Users have friends
    -   Posts have comments
    -   Comments have likes from users
    -   Users share and like posts...

-   Highly available across 3 AZ, with up to 15 read replicas

-   Build and run applications working with highly connected datasets --
    optimized for these complex and hard queries

-   Can store up to billions of relations and query the graph with
    milliseconds latency

-   Highly available with replications across multiple AZs

-   Great for knowledge graphs (Wikipedia), fraud detection,
    recommendation engines, social networking

Amazon QLDB(Quantum Ledger Database)

-   완전관리형 원장 데이터베이서로, 투명하고 변경불가능하며, 암호화
    방식으로 검증가능한 트랜잭션 로그를 제공

```{=html}
<!-- -->
```
-   A ledger is a book recording financial transactions
-   Fully Managed, Serverless, High available, Replication across 3 AZ
-   Used to review history of all the changes made to your application
    data over time
-   Immutable system: no entry can be removed or modified,
    cryptographically verifiable
-   Difference with Amazon Managed Blockchain: no decentralization
    component, in accordance with financial regulation rules

Amazon Managed Blockchain

-   Blockchain makes it possible to build applications where multiple
    parties can execute transactions without the need for a trusted,
    central authority.

-   Amazon Managed Blockchain is a managed service to:

    -   Join public blockchain networks
    -   Or create your own scalable private network

-   Compatible with the frameworks Hyperledger Fabric & Ethereum

AWS Glue

-   서버리스 데이터 통합 서비스, 이벤트 주도 ETL(추출, 변형 및 로드)
    파이프라인
-   Managed extract, transform, and load (ETL) service
-   Useful to prepare and transform data for analytics
-   Fully serverless service
-   Glue Data Catalog: catalog of datasets : can be used by Athena,
    Redshift, EMR

Databases & Analytics Summary in AWS

-   Relational Databases - OLTP: RDS & Aurora (SQL)
-   Differences between Multi-AZ, Read Replicas, Multi-Region
-   In-memory Database: ElastiCache
-   Key/Value Database: DynamoDB (serverless) & DAX (cache for DynamoDB)
-   Warehouse - OLAP: Redshift (SQL)
-   Hadoop Cluster: EMR
-   Athena: query data on Amazon S3 (serverless & SQL)
-   QuickSight: dashboards on your data (serverless)
-   DocumentDB: "Aurora for MongoDB" (JSON -- NoSQL database)
-   Amazon QLDB: Financial Transactions Ledger (immutable journal,
    cryptographically verifiable)
-   Amazon Managed Blockchain: managed Hyperledger Fabric & Ethereum
    blockchains
-   Glue: Managed ETL (Extract Transform Load) and Data Catalog service
-   Database Migration: DMS • Neptune: graph database

ECS (Elastic Container Service)

-   launch docker containers on AWS
-   you must provision & maintain the infrastructure (the EC2 Instances)
-   AWS takes care of starting / stopping containers
-   Has integration with the application load balancer

![](Pictures/100000010000022E000002379872DE5336B38947.png){width="2.1929in"
height="2.228in"}

Fargate

-   Launch Docker containers on AWS

-   You do not provision the infrastructure (no EC2 instances to manger)
    =\> simpler!

-   Serverless offering

-   AWS just runs containers for you based on the CPU / RAM you need

    ![](Pictures/100000010000021A000001C971214AD354CA6125.png){width="2.7228in"
    height="2.3134in"}

ECR (Elastic Container Registry)

-   Private docker registry on AWS
-   This is where you store your docker images so they can run by ECS or
    Fargate

AWS lambda

-   서버를 프로지버닝 또는 관리하지않고도 실제로 모든 유횽의
    애플리케이션 또는 백엔드 서비스에 대한 코드를 실행할 수 있는 이벤트
    중심의 서버리스 컴퓨팅 서비스
-   Lambda is Serverless, Function as a Service, seamless scaling,
    reactive

![](Pictures/10000001000003DC000001CC92DF7FBF0268122B.png){width="4.4217in"
height="2.0583in"}

-   Easy pricing: pay per request and compute time
-   It's very cheap to run AWS Lamda
-   Integrated with the whole AWS suite of services
-   Event-Driven: functions get invoked by AWS when needed
-   Integrated with many programming languages
-   easy monitoring through AWS CloudWatch
-   easy to get more resources per functions
-   Increasing RAM will also import CPU and network

API Gateway

-   Fully managed service for developers to easily create, public,
    maintain, monitor, and secure APIs
-   serverless and scalable
-   supports RESTful APIs and WebSocket APIs
-   support for security, user authentication, API throttling, API keys,
    monitoring

AWS Batch

-   Fully managed batch processing at any scale
-   Efficiently run 100,000s of computing batch jobs on AWS
-   A "batch" job is a job with a start and an end (opposed to
    continuous)
-   Batch will dynamically launch EC2 instances or Spot Instances
-   AWS Batch provisions the right amount of compute / memory
-   You submit or schedule batch jobs and AWS Batch does the rest!
-   Batch jobs are defined as Docker images and run on ECS
-   Helpful for cost optimizations and focusing less on the
    infrastructure

Batch vs Lambda

![](Pictures/10000001000003A7000001B09FCAFBF6530F245B.png){width="4.9382in"
height="2.2811in"}

Amazon Lightsail

-   저렴한 비용의 사전 구성된 클라우드 리소스를 통해 애플리케이션 및
    웹사이트를 빠르게 구축

```{=html}
<!-- -->
```
-   Virtual servers, storage, databases, and networking

-   Low & predictable pricing

-   Simpler alternative to using EC2, RDS, ELB, EBS, Route 53...

-   Great for people with little cloud experience!

-   Can setup notifications and monitoring of your Lightsail resources

-   Use cases:

    -   Simple web applications (has templates for LAMP, Nginx, MEAN,
        Node.js...)
    -   Websites (templates for WordPress, Magento, Plesk, Joomla)
    -   Dev / Test environment
    -   Has high availability but no auto-scaling, limited AWS
        integrations

CloudFormation (Infrastructure as a Code)

-   CloudFormation is a declarative way of outlining your AWS
    infrastructure, for any resources

-   For example, within a CloudFormation template, you say:

    -   I want a security group
    -   I want 2 EC2 instances using this security group
    -   I want an S3 bucket
    -   I want a ELB(load balancer) in front of these machines

-   Then CloudFormation creates those for you, in the right order, with
    the exact configuration that you specify

-   Benefits:

    -   Infrastructure as a Code

        -   No resources are manually created, which is excellent for
            control
        -   changes to the infrastructure are reviewed through code

    -   Cost

        -   each resources within the stack is tagged with an identifier
            so you can easily see how much a stack costs you
        -   Free(you pay for resources created)

    -   Productivity

        -   Ability to destroy and re-create an infrastructure on the
            cloud on the fly
        -   Automated generation of Diagram for your templates!
        -   Declarative programming (no need to figure out ordering and
            orchestration)

    -   Don't re-invent the wheel

        -   Leverage existing templates on the web!
        -   Leverage the documentation

    -   Supports (almost) all AWS resources:

        -   Everything we'll see in this course is supported
        -   You can use "custom resources" for resources that are not
            supported

CloudFormation Stack Designer

-   You can see the all the resources
-   You can see the relations between the components

AWS Cloud Development Kit (CDK)

-   Define your cloud infrastructure using a familiar language:

    -   JavaScript/TypeScript, Python, Java, and .NET

-   The code is "compiled" into a CloudFormation template (JSON/YAML)

-   You can therefore deploy infrastructure and application runtime code
    together

    -   Great for Lambda functions
    -   Great for Docker containers in ECS / EKS

![](Pictures/1000000100000418000001109A31A9AA5B1E54A8.png){width="5.0937in"
height="1.3217in"}

-   CDK Example

![](Pictures/100000010000020A000001C8B76B73880A5ADC22.png){width="3.1091in"
height="2.7161in"}

AWS Elastic Beanstalk (Platform as a Service: you only manage
application and data)

-   Elastic Beanstalk is a developer centric view of deploying an
    application on AWS

-   It uses all the component's we've seen before: EC2, ASG, ELB, RDS,
    etc...

-   But it's all in one view that's easy to make sense of!

-   We still have full control over the configuration

-   Beanstalk is free but you pay for the underlying instances

-   Managed service

    -   Instance configuration / OS is handled by Beanstalk
    -   Deployment strategy is configurable but performed by Elastic
        Beanstalk
    -   Capacity provisioning
    -   Load balancing & auto-scaling
    -   Application health-monitoring & responsiveness

-   Just the application code is the responsibility of the developer

-   Three architecture models:

    -   Single Instance deployment: good for dev
    -   LB + ASG: great for production or pre-production web
        applications
    -   ASG only: great for non-web apps in production (workers, etc..)

AWS CodeDeploy

-   We want to deploy our application automatically
-   Works with EC2 Instances & On-Premises Servers
-   Servers / Instances must be provisioned and configured ahead of time
    with the CodeDeploy Agent

AWS CodeCommit

-   Source-control service that hosts Git-based repositories
-   The code changes are automatically versioned

AWS CodeBuild

-   소스코드를 컴파일하는 단계부터 테스트 실행 후 소프트웨어 패키지를
    개발하여 배포하는 단계까지 마칠수 있는 완전관리형의 지속적
    통합서비스
-   Compiles source code, run tests, and produces packages that are
    ready to be deployed (by CodeDeploy for example)
-   serverless
-   only pay for the build time

AWS CodePipeline

-   코드 변경이 발생할 때마다 사용자가 정의한 릴리스 모델을 기반으로
    릴리스 프로세스의 빌드, 테스트 및 배포단계를 자동화

```{=html}
<!-- -->
```
-   Orchestrate the different steps to have the code automatically
    pushed to production

    -   Code =\> Build =\> Test =\> Provision =\> Deploy
    -   Basis for CICD

![](Pictures/100000010000038A000000E3320E10BB9DF0A45A.png){width="6.2957in"
height="1.5772in"}

AWS CodeArtifact

-   Software packages depend on each other to be built (also called code
    dependencies), and new ones are created
-   Storing and retrieving these dependencies is called artifact
    management
-   Traditionally you need to setup your own artifact management system
-   Works with common dependency management tools such as Maven, Gradle,
    npm, yarn, twine, pip, and NuGet

AWS CodeStar

-   Unified UI to easily manage software development activities in one
    place
-   "Quick way" to get started to correctly set-up CodeCommit,
    CodePipeline, CodeBuild, CodeDeploy, Elastic Beanstalk, EC2, etc...
-   Can edit the code "in-the-cloud" using AWS Cloud9

AWS Cloud9

-   AWS Cloud9 is a cloud IDE (Integrated Development Environment)
-   A cloud IDE can be used within a web browser, meaning you can work
    on your projects from anywhere with internet with no setup necessary

AWS Systems Manager (SSM)

-   Helps you manage your EC2 and On-Premises systems at scale

-   Another Hybrid AWS service

-   Get operational insights about the state of your infrastructure

-   Suite of 10+ products

-   Most important features are:

    -   Patching automation for enhanced compliance
    -   Run commands across an entire fleet of servers
    -   Store parameter configuration with the SSM Parameter Store
    -   Works for both Windows and Linux OS

AWS OpsWorks

-   Chef & Puppet help you perform server configuration automatically,
    or repetitive actions

-   They work great with EC2 & On-Premises VM

-   AWS OpsWorks = Managed Chef & Puppet

-   It's an alternative to AWS SSM

-   Only provision standard AWS resources:

    -   EC2 Instances, Databases, Load Balancers, EBS volumes...

-   In the exam: Chef or Puppet needed =\> AWS OpsWorks

Deployment - Summary

-   CloudFormation: (AWS only)

    -   Infrastructure as Code, works with almost all of AWS resources
    -   Repeat across Regions & Accounts

-   Beanstalk: (AWS only)

    -   Platform as a Service (PaaS), limited to certain programming
        languages or Docker
    -   Deploy code consistently with a known architecture: ex, ALB +
        EC2 + RDS

-   CodeDeploy (hybrid): deploy & upgrade any application onto servers

-   Systems Manager (hybrid): patch, configure and run commands at scale

-   OpsWorks (hybrid): managed Chef and Puppet in AWS

Developer Services - Summary

-   CodeCommit: Store code in private git repository (version
    controlled)
-   CodeBuild: Build & test code in AWS
-   CodeDeploy: Deploy code onto servers
-   CodePipeline: Orchestration of pipeline (from code to build to
    deploy)
-   CodeArtifact: Store software packages / dependencies on AWS
-   CodeStar: Unified view for allowing developers to do CICD and code
-   Cloud9: Cloud IDE (Integrated Development Environment) with collab
-   AWS CDK: Define your cloud infrastructure using a programming
    language

Why make a global application?

-   A global application is an application deployed in multiple
    geographies

-   On AWS: this could be Regions and / or Edge Locations

-   Decreased Latency

    -   Latency is the time it takes for a network packet to reach a
        server
    -   It takes time for a packet from Asia to reach the US
    -   Deploy your applications closer to your users to decrease
        latency, better experience

-   Disaster Recovery (DR)

    -   If an AWS region goes down (earthquake, storms, power shutdown,
        politics)...
    -   You can fail-over to another region and have your application
        still working
    -   A DR plan is important to increase the availability of your
        application

-   Attack protection: distributed global infrastructure is harder to
    attack

Global AWS Infrastructure

-   Regions: For deploying applications and infrastructure
-   Availability Zones: Made of multiple data centers
-   Edge Locations (Points of Presence): for content delivery as close
    as possible to users

Global Applications in AWS

-   Global DNS: Route 53

    -   Great to route users to the closest deployment with least
        latency
    -   Great for disaster recovery strategies

-   Global Content Delivery Network (CDN): CloudFront

    -   Replicate part of your application to AWS Edge Locations --
        decrease latency
    -   Cache common requests -- improved user experience and decreased
        latency

-   S3 Transfer Acceleration

    -   Accelerate global uploads & downloads into Amazon S3

-   AWS Global Accelerator:

    -   Improve global application availability and performance using
        the AWS global network

Amazon Route 53 Overview

-   Route53 is a Managed DNS (Domain Name System)

-   DNS is a collection of rules and records which helps clients
    understand how to reach a server through URLs.

-   In AWS, the most common records are:

    -   www.google.com =\> 12.34.56.78 == A record (IPv4)
    -   www.google.com =\> 2001:0db8:85a3:0000:0000:8a2e:0370:7334 ==
        AAAA IPv6
    -   search.google.com =\> www.google.com == CNAME: hostname to
        hostname
    -   example.com =\> AWS resource == Alias (ex: ELB, CloudFront, S3,
        RDS, etc...)

Route 53 -- Diagram for A Record

![](Pictures/100000010000030A00000213FFFAF7F5D49EEB60.png){width="3.8173in"
height="2.6055in"}

Route 53 Routing Policies

![](Pictures/100000010000042500000192A4B33392D57F13EE.png){width="6.9252in"
height="2.6228in"}![](Pictures/100000010000042900000196044CC69BEDF20172.png){width="6.9252in"
height="2.6398in"}

Amazon CloudFront

-   Content Delivery Network (CDN)
-   Improves read performance, content is cached at the edge
-   Improves users experience
-   216 Point of Presence globally (edge locations)
-   DDoS protection (because worldwide), integration with Shield, AWS
    Web Application Firewall

CloudFront -- Origins

-   S3 bucket

    -   For distributing files and caching them at the edge •
    -   Enhanced security with CloudFront Origin Access Control (OAC)
    -   OAC is replacing Origin Access Identity (OAI)
    -   CloudFront can be used as an ingress (to upload files to S3)

-   Custom Origin (HTTP)

    -   Application Load Balancer
    -   EC2 instance
    -   S3 website (must first enable the bucket as a static S3 website)
    -   Any HTTP backend you want

CloudFront at a high level

![](Pictures/100000010000040B000001B352D6F358A8228FFC.png){width="5.7236in"
height="2.4047in"}

CloudFront --
![](Pictures/100000010000035D0000020A7298CD9A2F3F4184.png){width="4.3937in"
height="2.6634in"}S3 as an Origin

CloudFront vs S3 Cross Region Replication

-   CloudFront:

    -   Global Edge network
    -   Files are cached for a TTL (maybe a day)
    -   Great for static content that must be available everywhere

-   S3 Cross Region Replication:

    -   Must be setup for each region you want replication to happen
    -   Files are updated in near real-time
    -   Read only
    -   Great for dynamic content that needs to be available at
        low-latency in few regions

S3 Transfer Acceleration

-   Increase transfer speed by transferring file to an AWS edge location
    which will forward the data to the S3 bucket in the target region

![](Pictures/10000001000002FA000000D9341F73704427C7F5.png){width="6.3508in"
height="1.8083in"}

AWS Global Accelerator

-   Improve global application availability and performance using the
    AWS global network
-   Leverage the AWS internal network to optimize the route to your
    application (60% improvement)
-   2 Anycast IP are created for your application and traffic is sent
    through Edge Locations
-   The Edge locations send the traffic to your application

![](Pictures/10000001000001EA000001D8E693297985F20B36.png){width="3.2709in"
height="3.1508in"}

AWS Global Accelerator

![](Pictures/10000001000003BE000001AF7E2D68E169378569.png){width="5.5654in"
height="2.5035in"}

AWS Global Accelerator vs CloudFront

-   They both use the AWS global network and its edge locations around
    the world

-   Both services integrate with AWS Shield for DDoS protection.

-   CloudFront -- Content Delivery Network

    -   Improves performance for your cacheable content (such as images
        and videos)
    -   Content is served at the edge

-   Global Accelerator

    -   No caching, proxying packets at the edge to applications running
        in one or more AWS Regions.
    -   Improves performance for a wide range of applications over TCP
        or UDP
    -   Good for HTTP use cases that require static IP addresses
    -   Good for HTTP use cases that required deterministic, fast
        regional failover

AWS Outposts

-   Hybrid Cloud: businesses that keep an on - premises infrastructure
    alongside a cloud infrastructure. Therefore, two ways of dealing
    with IT systems:

    -   One for the AWS cloud (using the AWS console, CLI, and AWS APIs)
    -   One for their on -premises infrastructure

-   AWS Outposts are "server racks" that offers the same AWS
    infrastructure, services, APIs & tools to build your own
    applications on -premises just as in the cloud

-   AWS will setup and manage "Outposts Racks" within your on -premises
    infrastructure and you can start leveraging AWS services on-premises

-    You are responsible for the Outposts Rack physical security

![](Pictures/10000001000002100000011336E2EB3C94B1DD42.png){width="4.0783in"
height="1.8575in"}

AWS Outposts

-   Benefits:

    -   Low-latency access to on-premises systems
    -   Local data processing
    -   Data residency
    -   Easier migration from on-premises to the cloud
    -   Fully managed service

AWS WaveLength

-   WaveLength Zones are infrastructure deployments embedded within the
    telecommunications providers' datacenters at the edge of the 5G
    networks
-   Brings AWS services to the edge of the 5G networks
-   Example: EC2, EBS, VPC...
-   Ultra-low latency applications through 5G networks
-   Traffic doesn't leave the Communication Service Provider's (CSP)
    network
-   High-bandwidth and secure connection to the parent AWS Region
-   No additional charges or service agreements
-   Use cases: Smart Cities, ML-assisted diagnostics, Connected
    Vehicles, Interactive Live Video Streams, AR/VR, Real-time Gaming,
    ...

```{=html}
<!-- -->
```

AWS Local Zones

-   Places AWS compute, storage, database, and other selected AWS
    services closer to end users to run latency-sensitive applications

-   Extend your VPC to more locations -- "Extension of an AWS Region"

-   Compatible with EC2, RDS, ECS, EBS, ElastiCache, Direct Connect ...

-   Example:

    -   AWS Region: N. Virginia (us-east-1)
    -   AWS Local Zones: Boston, Chicago, Dallas, Houston, Miami, ...

Global Applications Architecture

![](Pictures/10000001000003E9000001E1AC5C5FA59E2C2A3A.png){width="5.9555in"
height="2.861in"}

![](Pictures/1000000100000403000001D621B1AD16F02C1EBB.png){width="6.6091in"
height="3.0244in"}

Global Applications in AWS - Summary

-   Global DNS: Route 53

    -   Great to route users to the closest deployment with least
        latency
    -   Great for disaster recovery strategies

-   Global Content Delivery Network (CDN): CloudFront

    -   Replicate part of your application to AWS Edge Locations --
        decrease latency
    -   Cache common requests -- improved user experience and decreased
        latency

-   S3 Transfer Acceleration

    -   Accelerate global uploads & downloads into Amazon S3

-   AWS Global Accelerator

    -   Improve global application availability and performance using
        the AWS global network

-   AWS Outposts

    -   Deploy Outposts Racks in your own Data Centers to extend AWS
        services

-   AWS WaveLength

    -   Brings AWS services to the edge of the 5G networks
    -   Ultra-low latency applications

-   AWS Local Zones

    -   Bring AWS resources (compute, database, storage, ...) closer to
        your users
    -   Good for latency-sensitive applications

Cloud Integration Section

-   When we start deploying multiple applications, they will ineveitably
    need to communicate with one another
-   There are two paterns of application communication

![](Pictures/10000001000003F7000000D6978CDAD4183AE41C.png){width="6.3945in"
height="1.348in"}

-   Syncoronos between applications can be problematic if there are
    sudden spikes of traffic

-   what if you need to suddenly encode 1000 videos but usually it's 10?

-   in that case, it's better to decouple you applications:

    -   using SQS: queue model
    -   using SNS: pub/sub model
    -   using Kinesis: real-time data streaming model

-   These services can scale independently from our application

Aamazon SQS (Simple Queue Service)

-    Fully managed service (=serverless), use to decouple applications

-   Default retention of messages: 4days, maximum of 14days

-   No limit to how many messages can be in the queue

-   Messages are deleted after they're read by consumers

-   Consumers share the work to read messages & scale horizontally

    ![](Pictures/10000001000003BC00000179DF15240E77319B6C.png){width="4.3138in"
    height="1.7016in"}

Amazon Kinesis

-   Kinesis = real-time big data streaming
-   Managed service to collect, process, and analyze real-time streaming
    data at any scale

Amazon SNS

-   What if you want to send one message to many receivers?

![](Pictures/1000000100000408000001AA95466BC0B2FD3C24.png){width="4.0311in"
height="1.6638in"}

Amazon MQ

-   SQS, SNS are "cloud-native" services: proprietary protocols from AWS
-   When migrating to the cloud, instead of re-engineering the
    application to use SQS and SNS, we can use Amazon MQ
-   Amazon MQ is a managed message broker service for RabiitMQ/ActiveMQ
-   Amazon MQ doesn't "scale" as much as SQS/SNS
-   Amazon MQ runs on servers, can run in multi-AZ with failover
-   Amazon MQ has both queue feature(SQS) and topic features(SNS)

Integration Section -- Summary

-   SQS:

    -   Queue service in AWS
    -   Multiple Producers, messages are kept up to 14days
    -   Multipe Consumers share the read and delete messages when done
    -   Used to decouple applications in AWS

-   SNS:

    -   Notification service in AWS
    -   Subscribers: Email, Lamda, SQS, HTTP, Mobile...
    -   Multiple Subscribers, send all messages to all of them
    -   No message retention

-   Kinesis: real-time data streaming, persistence and analysis

-   Amazon MQ: managed message broker for ActiveMQ and RabbitMQ in the
    cloud

Amazon CloudWatch Metrics

-   CloudWatch provides metrics for every services in AWS

-   Important Metrics

    -   EC2 instances: CPU utilization, status checks, network(not RAM)
    -   EBS volumes: Disk Read/Writes
    -   S3 buckets: BucketSizeBytes, NumberOfObjects, AllRequests
    -   Billing: Total Estimated Charge
    -   Service Limits: how much you've been using a service API
    -   custom metrics: push your own metrics

Amazon CloudWatch Alarms

-   CloudWatch logs can collect log from

    -   Elastic Beanstalk: collection of logs from application
    -   ECS: collection from containers
    -   AWS Lamda: collection from function logs
    -   CloudTrail based on filter
    -   CloudWatch log agents: on EC2 machines or on-premises servers
    -   Route53: Log DNS queries

-   Enables real-time monitoring of logs

-   Adjustable CloudWatch logs retention

CloudWatch Logs for EC2

-   By default, no logs from your EC2 instance will go to CloudWatch
-   You need to run a CloudWatch agent on EC2 to push the log files you
    want
-   Make sure IAM permissions are correct
-   The CloudWatch log agent can be setup on-premises too

Amazon EventBridge

-   Schedule: Cron jobs (scheduled scripts)
-   Event Pattern: Event rules to react to a service doing something
-   Trigger Lambda functions, send SQS/SNS messages
-   Schema Registry: model event schema
-   You can archive events (all/filter) sent to an event bus
    (indefinitely or set period)
-   Ability to replay archived events

AWS CloudTrail

-   Provides governance, compliance and audit for your AWS Account
-   CouldTrail is enabled by default
-   Get an history of events / API calls made within your AWS Account
    by: Console/SDK/CLI
-   Can put logs from CloudTrail into CloudWatch Logs or S3
-   A trail can be applied to All Regions(default) or a single Region.
-   If a resource is deleted in AWS, investigate CloudTrail first!

CloudTrail Diagram

![](Pictures/100000010000043C000001B93F25EC85959B7B85.png){width="5.5102in"
height="2.2417in"}

AWS X-Ray: Visual analysis of our applications

-   Debugging in Production, the good old way:

    -   Test locally
    -   Add log statements everywhere
    -   re-deploy in production

-   Advantages

    -   Troubleshooting performance(bottlenecks)
    -   Understand dependencies in a microservice architecture
    -   Pinpoint service issues
    -   Review request behavior
    -   Find errors and exceptions

Amazon CodeGuru

-   AM ML-powered service for automated code reviews and application
    performance recommendations

-   Provide two functionalities

    -   CodeGuru Reviewer: automated code reviews for static code
        analysis (development)

        -   Identify critical issues, security vulnerabilities, and
            hard-to-find bugs
        -   ex) common coding best practices, resource leaks, security
            detection, input validation
        -   Uses Machine Learning and automated reasoning
        -   Hard-learned lessons across milions of code reviews on 1000s
            of open-source and Amazon repositories
        -   Supports Java and Python
        -   Integrates with GitHub, Bitbucket, and AWS CodeCommit

    -   CodeGuru Profiler: visibility/recommendations about application
        performance during runtime (production)

        -   Helps understand the runtime behavior of your application

        -   ex) identify if your application is consuming excessive CPU
            capacity on a logging routine

        -   Features:

            -   identify and remove code inefficiencies
            -   improve application performance(reduce CPU utilization)
            -   decrease compute costs
            -   provide heap summary(identify which objects using up
                memory)

        -   Support applications running on AWS or on-premise

![](Pictures/10000001000003E9000000E374000ABAEE1628C4.png){width="6.9252in"
height="1.5701in"}

AWS Health Dashboard

-   Shows all regions, all services health
-   Shows historical information for each day
-   Has an RSS feed you can subscribe to
-   AWS Account Health Dashboard provides alerts and remediation
    guidance when AWS is experiencing events that may impcat you
-   While the service health dashboard displays the general status of
    AWS services, Account Health Dashboard gives you a personalized view
    into the performance and availability of the AWS services underlying
    your AWS resources
-   The dashboard displays relevant and timely information to help you
    manage events in progress and provides proactive notification to
    help you plan for scheduled activities
-   Can aggregate data from an entire AWS Organization

Monitoring Summary

-   CloudWatch:

    -   Metrics: monitor the performance of AWS services and billing
        metrics
    -   Alarms: automate notification, perform EC2 action, notify to SNS
        based on metric
    -   Logs: collect log files from EC2 instances, servers, Lamda
        functions
    -   Events(or EventBridge): react to events in AWS, or trigger a
        rule on a schedule

-   CloudTrail: audit API calls made within your AWS account

-   CloudTrail Insights: automated analysis of your CloudTrail Events

-   X-Ray: trace requests made through your distributed applications

-   Service Health Dashboard: status of all AWS services across all
    regions

-   Personal Health Dashboard: AWS events that impact you infrastructure

-   Amazon CodGuru: automated code reviews and application performance
    recommandations

![](Pictures/100000010000015C000002054744697AEB78849B.png){width="1.5992in"
height="2.3756in"}

VPC & Subnets Primer

-   VPC -- Virtual Private Cloud: private network to deploy your
    resources (regional resource)
-   Subnets allow you to partition your network inside your VPC
    (Availability Zone resouces)
-   A public subnet is a subnet that is accessible from the internet
-   A private subnet is a subnet that is not accessible from the
    internet
-   To define access to the internet and between subnets, we use
    RouteTables

VPC Diagram

![](Pictures/10000001000002DD000001E85EB872F0C61E1951.png){width="4.2425in"
height="2.8244in"}

Internet Gateway & NAT
Gateways![](Pictures/10000001000002010000028105D81BF4E6F7FD06.png){width="1.9043in"
height="2.3791in"}

-   Internet Gateways helps our VPC instances connect with the internet

-   Public subnets have a route to the internet gateway

-   NAT Gateways (AWS-managed) & NAT Instances(self-managed) allow your
    instances in your Private Subnets to access the internet while
    remaining private

![](Pictures/100000010000016F000002040B1C5082B565A78E.png){width="1.7807in"
height="2.5028in"}

Network ACL & Security Groups

-   NACL(Network ACL)

    -   A firewall which controls traffic from and to subnet
    -   Can have ALLOW and DENY rules
    -   Are attached at the Subnet level
    -   Rules only include IP addresses

-   Security Groups

    -   A firewall that controls traffic to and from an ENI / an EC2
        Instance
    -   Can have only ALLOW rules
    -   Rules include IP addresses and other security groups

Network ACLs vs Security Groups

![](Pictures/10000001000003A7000001AC1CAF5BAC0383A96E.png){width="5.702in"
height="2.6098in"}

VPC Flow Logs

-   Capture information about IP traffic going into your interfaces:

    -   VPC Flow Logs
    -   Subnet Flow Logs
    -   Elastic Network Interface Flow Logs

-   Helps to monitor & troubleshoot connectivity issues

-   Captures network information from AWS managed interfaces too: ELB,
    ElasticCache,RDS

-   VPC Flow logs data can go to S3 / CloudWatch Logs

VPC
Peering![](Pictures/10000001000001AB00000161A5DBE9FF283094D8.png){width="1.9508in"
height="1.6126in"}

-   Connect two VPC, privately using AWS network
-   Make them behave as if they were in the same network
-   Must not have overlapping CIDR (IP address range)
-   VPC Peering connection is not transitive (must be established for
    each VPC that need to communicate with one another)

![](Pictures/1000000100000182000001D18FC4EC1212A21869.png){width="1.8091in"
height="2.1791in"}

VPC Endpoints

-   Endpoints allow you to connect to AWS Services using a private
    network instead of the public www network
-   This gives you enhanced security and lower latency to access AWS
    services
-   VPC Endpoint Gateway: S3 & DynamoDB
-   VPC Endpoint Interface: the rest

AWS PrivateLink (VPC Endpoint Services)

-   Most secure & scalable way to expose a service to 1000s of VPCs
-   Does not require VPC peering, internet gateway, NAT, route tables
-   Requires a network load balancer (Service VPC) and ENI (Customer
    VPC)

![](Pictures/100000010000036400000148E3E00F6DD2A2D307.png){width="5.7874in"
height="2.1862in"}

Site to Site VPN & Direct
Connect![](Pictures/10000001000001BE00000159A30A2FCF8124E0EA.png){width="2.0319in"
height="1.5717in"}

-   Site to Site VPN

    -   connect an on-premises VPN to AWS
    -   The connection is automatically encrypted
    -   Goes over the public internet
    -   On-premises: must use a Customer Gateway (CGW)
    -   AWS: must use a Virtual Private Gateway
        (VGW)![](Pictures/1000000100000438000000A781352A7B181F5E0F.png){width="6.2591in"
        height="0.9673in"}

-   Direct Connect (DX)

    -   Establish a physical connection between on-premises and AWS
    -   The connection is private, secure and fast
    -   Goes over a private network
    -   Takes at least a month to establish

AWS ClientVPN

-   Connect from your computer using OpenVPN to your private network in
    AWS and on-premises
-   Allow you to connect to your EC2 instances over a priviate IP (just
    as if you were in the private VPC network)
-   Goes over public Internet

![](Pictures/1000000100000350000000E757D5CB8724BCC2CF.png){width="4.9791in"
height="1.3563in"}

Transit
Gateway![](Pictures/10000001000001AA000001B44F916FF7C12871A4.png){width="2.2091in"
height="2.261in"}

-   For having transitive peering between thousands of VPC and
    on-premises, hub-and-spoke(star) connection
-   One single gateway to provide this functionality
-   Works with Direct Connect Gateway, VPN connections

VPC -- Summary

-   VPC: Virtual Private Cloud
-   Subnets: Tied to an AZ, network partition of the VPC
-   Internet Gateway: at the VPC level, provide Internet Access
-   NAT Gatway / Instances: give internet access to private subnets
-   NACL: Stateless, subnet rules for inbound and outbound
-   Security Groups: Stateful, operate at the EC2 instance level or ENI
-   VPC Peering: Connect two VPC with non overlapping IP ranges,
    nontransitive
-   VPC Endpoints: Provide private access to AWS Services within VPC
-   PrivateLink: Privately connect to a service in a 3^rd^ party VPC
-   VPC Flow Logs: network traffic logs
-   Site to Site VPN: VPN over public internet between on-premises DC
    and AWS
-   Client VPN: OpenVPN connection from your computer into your VPC
-   Direct Connect: direct private connection to AWS
-   Transit Gateway: Conenction thousands of VPC and on-premises
    networks together

![](Pictures/10000001000002FE000001A630EDE8B28FAB9CC5.png){width="3.2965in"
height="1.8161in"}

DDOS Protection on AWS

-   AWS Shield Standard

    -   protects against DDOS attack for your website and applications,
        for all customers at no additional costs
    -   Provides protection from attacks such as SYN/UDP Floods,
        Reflection attacks and other layer 3/layer 4 attacks

-   AWS Shield Advanced:

    -   Optional DDOS mitigation service (\$3,000 per month per
        organization)
    -   Protect against more sophisticated attack on EC2, ELB,
        CloudFront, Route53
    -   24/7 premium DDOS protection

-   AWS WAF (Web Application Firewall): Filter specific requests based
    on rules

    -   Protects your web applications from common web exploits(Layer 7)

    -   Layer 7 is HTTP(vs Layer 4 is TCP)

    -   Deploy on Application Load Balancer, API Gateway, CloudFront

    -   Define Web ACL(Web Access Control List)

        -   Rules can include IP addresses, HTTP headers, HTTP body, or
            URI strings
        -   Protects from common attack: SQL Injection and Cross-Site
            Scripting(XSS)
        -   Size constraints, geo-math(block countries)
        -   Rate-based rules (to count occurrences of events) -- for
            DDOS protection

-   CloudFront and Route 53

    -   Availability protection using global edge network
    -   Combined with AWS shield, provides attack mitigation at the edge

-   Be ready to scale -- leverage AWS Auto Scaling

AWS KMS (Key Management Service)

-   Anytime you hear "encryption" for an AWS service, it's most likely
    KMS

-   KMS = AWS manages the encryption keys for us

-   Encrption Opt-in:

    -   EBS volumes: encrypt volumes
    -   S3 buckets: Server-side encryption of objects
    -   Redshift database: encryption of data
    -   RDS database: encryption of data
    -   EFS drives: encryption of data

-   Encryption Automatically enabled:

    -   CloudTrail Logs
    -   S3 Glacier
    -   Storage Gateway

CloudHSM![](Pictures/1000000100000160000000B18E76A65CB07D6546.png){width="2.0217in"
height="1.0165in"}

-   KMS =\> AWS manages the software for encryption
-   CloudHSM =\> AWS provisions encryption hardware
-   Dedicated Hardware (HSM = Hardware Security Module)
-   You manage your own encryption keys entirely (Not AWS)

Types of Customer Master Keys: CMK

-   Customer Managed CMK

    -   create, manage and used by the customer, can enable or disable
    -   Possibility of rotation policy(new key generated every year, old
        key preserved)
    -   Possibility to bring-your-own-key

-   AWS managed CMK:

    -   Created, managed and used on the customer's behalf by AWS
    -   Used by AWS services (S3, EBS, Redshift)

-   AWS owned CMK:

    -   Collection of CMKs that an AWS service owns and manages to use
        in multiple accounts
    -   AWS can use those to protect resources in your account (but you
        can't view the keys)

-   CloudHSM keys (custom keystore):

    -   Keys generated from your own CloudHSM hardware device
    -   Cryptographic operations are performed within the CloudHSM
        cluster

AWS Certificate Manager (ACM)

-   Let's you easily provision, manage, and deploy SSL/TLS Certificates

-   Used to provide in-flight encryption for websites (HTTPS)

-   Supports both public and private TLS certificates

-   Free of charge for public TLS certificates

-   Automatic TLS certificate renewal

-   Integrations with (load TLS certificates on)

    -   ELB(Elastic Load Balancers)
    -   CloudFront Distributions
    -   APIs on API Gateway

AWS Secrets Manager

-   Capability to force rotation of secrets every X days
-   Automate generation of secrets on rotation (uses Lambda)
-   Integration with Amazon RDS (MySQL, PostgreSQL, Aurora)
-   Secrets are encrypted using KMS
-   Mostly meant for RDS Integration

AWS Artifact (not really a service)

-   Portal that provides customers with on-demand access to AWS
    compliance documentation and AWS agreements
-   Artifact Reports: Allows you to download AWS security and compliance
    documents from third-party auditors, like AWS ISO certifications,
    Payment Card Industry (PCI), and System and Organization Control
    (SOC) reports
-   Artifact Agreements: Allows you to review, accept, and track the
    status of AWS agreements such as the Business Associate Addendum
    (BAA) or the Health Insurance Portability and Accountability Act
    (HIPAA) for an individual account or in your organization
-   Can be used to support internal audit or compliance

Amazon GuardDuty

-   Intelligent Threat discovery to protect your AWS account

-   Uses Machine Learning algorithms, anomaly dection, 3^rd^ party data

-   One click to enable (30days trial), no need to install software

-   input data includeS:

    -   CloudTrail Events Logs -- unusual API calls, unauthroized
        deployments

        -   CloudTrail Management Events -- create VPC subnet, create
            trail
        -   CloudTrail S3 Data Events -- get object, list objects,
            delete object

    -   VPC Flow Logs -- unusual internal traffic, unusual IP address

    -   DNS Logs -- compromised EC2 instances sending encoded data
        within DNS queries

    -   Kubernetes Audit Logs -- suspicious activites and potential EKS
        cluster compromises

-   Can setup EventBridge rules to be notified in case of findings

-   EventBridge rules can target AWS Lambda or SNS

-   Can protect against CryptoCurrency attacks (has a dedicated
    "finding" for it)

Amazon Inspector

-   Automated Security Assessments

-   For EC2 instances

    -   Leveraging the AWS System Manager (SSM) agent
    -   Analyze against unintended network accessibility
    -   Analyze the running OS against known vulnerabilities

-   For Container Images push to Amazon ECR

    -   Assessment of functions as they are deployed

-   Reporting & Integration with AWS Security Hub

-   send findings to Amazon EventBridge

-   What does Amazon Inspector evaluate?

    -   Only for EC2 Instances, Container Images & Lamda functions
    -   Continuous scanning of the infrastructure, only when needed
    -   Package vulnerabilites (EC2, ECR & Lambda) -database of CVE
    -   network reachability (EC2)

AWS Config

-   Helps with auditing and recording compliance of your AWS resources

-   Helps record configurations and changes over time

-   Possiblity of storing the configuration data into S3

-   questions that can be solved by AWS Config:

    -   Is there unrestricted SSH access to my security groups?
    -   Do my buckets have any public access?
    -   How has my ALB configuration changed over time?

-   You can receive alerts (SNS Notifications) for any changes

-   AWS Config is a per-region service

-   Can be aggregated across regions and accounts

AWS Macie

-   fully managed data security and data privacy service that uses
    machine learning and pattern matching to discover and protect your
    sensitive data in AWS
-   Macie helps identify and alert you to sensitive data, such as
    personally identifiable information (PII)

![](Pictures/10000001000002570000008B5AC994D112476DEB.png){width="5.302in"
height="1.2307in"}

AWS Security Hub

-   Central security tool to manage security across several AWS accounts
    and automate security checks

-   Integrated dashboards showing current security and compliance status
    to quickly take actions

-   Automatically aggregates alerts in predefined or personal findings
    formats form various AWS services & AWS partner tools:

    -   GuardDuty, Inspector, Macie, IAM Access Analyzer, AWS Systems
        Manager, AWS Firewall Manager, AWS Partner Network Solutions

-   Must first enable the AWS Config Service

![](Pictures/10000001000002B700000137464908B7489805F5.png){width="4.4764in"
height="2.0028in"}

Amazon Detective

-   GuardDuty, Macie,and Security Hub are used to identify potential
    security issues or findings
-   Sometimes security findings require deeper analysis to isolate the
    root cause and take action -- it's a complex process
-   Amazon Detective analyzes, investigates, and quickly identifies the
    root cause of security issues or suspicious activities (using ML and
    graphs)
-   Automatically collects and processes events from VPC Flow Logs,
    CloudTrail, GuardDuty and create a unified view
-   Produces visualiztions with details and context to ge to the root
    cause

AWS Abuse

-   Report suspected AWS resources used for abusive or illegal purposes

-   Abusive & prohibited behaviors are:

    -   Spam -- receving undesired emails from AWS-owned IP address,
        websites & forums spammed by AWS resources
    -   Port scanning -- sending packets to your ports to discover the
        unsecured ones
    -   DoS or DDoS attacks -- AWS-owned IP addresses attempting to
        overwhlem or crash your servers/softwares
    -   Intrusion attempts -- logging in on your resources
    -   Hosting objectionable or copyrighted content -- distributing
        illegal or copyrighted content without consent
    -   Distributing malware -- AWS resources distributing softwares to
        harm computers or machines

Root User Privileges

-   Root user = Account Owner (created when the account is created)

-   Has complete access to all AWS services and resources

-   Lock away your AWS account root user access keys

-   Do not use the root account for everyday tasks, even administrative
    tasks

-   Actions that can be performed only by the root user

    -   Change account settings(account name, email address, root user
        password, root user access keys)
    -   View certain tax invoices
    -   Close your AWS account
    -   Restore IAM user permissions
    -   Change or cancel your AWS Support plan
    -   Register as a seller in the Reserved Instance Marketplace
    -   configure an Amazon S3 bucket to enable MFA
    -   Edit or delete an Amazon S3 bucket policy that includes an
        invalid VPC ID or VPC endpoint ID
    -   Sign up for GovCloud

Security & Compliance -- Summary

-   Shared Responsibility on AWS

-   Shield: Automatic DDOS protection + 24/7 support for advanced

-   WAF: Firwall to filter incoming requests based on rules

-   KMS: Encryption keys managed by AWS

-   CloudHSM: Hardware encryption, we manage encryption keys

-   AWS Certificate Manager: provision, manage, and deploy SSL/TLS
    certificates

-   Artifacts: Get access to compliance reports such as PCI, ISO

-   GuardDuty: Find malicious behavior with VPC, DNS & CloudTrail Logs

-   Inspector: find software vulnerabilities in EC2, ECR Images, and
    Lambda fuctions

-   Config: Track config changes and compilance against rules

-   Macie: Find sensitive data (ex:PII data) in Amazon S3 buckets

-   CloudTrail: Track API calls made by users within account

-   AWS Security Hub: gather security findings from mutliple AWS
    Accounts

-   Amazon Detective: find the root cause of security issues or
    suspicious activities

-   AWS Abuse: report AWS resources used for abusive or illegal purposes

-   Root User Privileges:

    -   change account settings
    -   close your AWS account
    -   change or cancel your AWS Support plan
    -   Register as a seller in the reserved instance marketplace

Machine Learning

Amazon Rekognition

-   Find objects, people, text, scenes in images and videos using
    Machine Learning

-   Facial analysis and facial search to do user verification, people
    counting

-   Create a database of "familiar faces" or compare against celebrities

-   Use cases:

    -   labeling, Content Moderation, Text Detection, Face Dection and
        Analysis, Face Search and Verification, Celebrity Recognition,
        Pathing(for sports game analysis)

Amazon Transcribe

-   Automatically convert speech to text

-   Uses a deep learning process called automatic speech recognition
    (ASR) to convert speech to text quickly and accurately

-   Automatically remove Personally Identifiable Information (PII) using
    Redaction

-   Supports Automatic Language Identification for multi-lingual audio

-   Use cases:

    -   transcribe customer service calls
    -   automate closed captioning and subtitling
    -   generate metadata for media assets to create a fully searchable
        archive

Amazon Polly

-   Turn text into lifelike speech using deep learning
-   Allowing you to create applications that talk

Amazon Translate

-   Natural and accurate language translation

```{=html}
<!-- -->
```
-   Amazon Translate allows you to localize content - such as websites
    and applications - for international users, and to easily translate
    large volumes of text efficiently.

Amazon Lex & Connect

-   Amazon Lex: (same technology that powers Alexa)

    -   Automatic Speech Recognition (ASR) to convert speech to text
    -   Natural Language Understanding to recognize the intent of text,
        callers
    -   Helps build chatbots, call center bots

-   Amazon Connect:

    -   Receive calls, create contact flows, cloud-based virtual contact
        center
    -   Can integrate with other CRM systems or AWS
    -   No upfront payments, 80% cheaper than traditional contact center
        solutions

![](Pictures/10000001000002EB000000657B74BB1C6B25C927.png){width="5.6925in"
height="0.7693in"}

Amazon Comprehend

-   For Natural Language Processing -- NLP

-   Fully managed and serverless service

-   Uses machine learning to find insights and relationships in text

    -   Language of the text
    -   Extracts key phrases, places, people, brands, or events
    -   Understands how positive or negative the text is
    -   Analyzes text using tokenization and parts of speech
    -   Automatically organizes a collection of text files by topic

-   Sample use cases:

    -   analyze customer interactions (emails) to find what leads to a
        positive or negative experience
    -   Create and groups articles by topics that Comprehend will
        uncover

Amazon SageMaker

-   Fully managed service for developers / data scientists to build ML
    models
-   Typically, difficult to do all the processes in one place +
    provision servers
-   Machine learning process (simplified): predicting your exam score

![](Pictures/1000000100000289000000D7DA4A505E6C14209C.png){width="5.2457in"
height="1.7374in"}

Amazon Forecast

-   Fully managed service that uses ML to deliver highly accurate
    forecasts
-   Example: predict the future sales of a raincoat
-   50% more accurate than looking at the data itself
-   Reduce forecasting time from months to hours
-   Use cases: Product Demand Planning, Financial Planning, Resource
    Planning, ...

![](Pictures/10000001000002F80000009031CE88E5289BE552.png){width="6.9252in"
height="1.3118in"}

Amazon Kendra

-   Fully managed document search service powered by Machine Learning
-   Extract answers from within a document (text, pdf, HTML, PowerPoint,
    MS Word, FAQs...)
-   Natural language search capabilities
-   Learn from user interactions/feedback to promote preferred results
    (Incremental Learning)
-   Ability to manually fine-tune search results (importance of data,
    freshness, custom, ...)

![](Pictures/10000001000002F7000000AE51DAA40ADFDE712A.png){width="6.9252in"
height="1.5866in"}

Amazon Personalize

-   Fully managed ML-service to build apps with real-time personalized
    recommendations
-   Example: personalized product recommendations/re-ranking, customized
    direct marketing
-   Example: User bought gardening tools, provide recommendations on the
    next one to buy
-   Same technology used by Amazon.com
-   Integrates into existing websites, applications, SMS, email
    marketing systems, ...
-   Implement in days, not months (you don't need to build, train, and
    deploy ML solutions)
-   Use cases: retail stores, media and entertainment...

![](Pictures/1000000100000304000000B8C4975E9D7B8170A3.png){width="6.3854in"
height="1.5217in"}

Amazon Textract

-   Automatically extracts text, handwriting, and data from any scanned
    documents using AI and ML • Extract data from forms and tables

-   Read and process any type of document (PDFs, images, ...)

-   Use cases:

    -   Financial Services (e.g., invoices, financial reports)
    -   Healthcare (e.g., medical records, insurance claims)
    -   Public Sector (e.g., tax forms, ID documents, passports)

![](Pictures/10000001000002A70000007D4999F3336F6D579C.png){width="6.0673in"
height="1.1165in"}

AWS Machine Learning -- Summary

-   Rekognition: face detection, labeling, celebrity recognition
-   Transcribe: audio to text(subtitles)
-   Polly: text to audio
-   Translate: translations
-   Lex: build conversational bots -- chatbots
-   Connect: cloud contact center
-   Comprehend: natural language processing
-   SageMaker: machine learning for every developer and data scientist
-   Forecast: build highly accurate forecasts
-   Kendra: ML-powered search engine
-   Personalize: real-time personalized recommendations
-   Textract: detect text and data in documents

Account Management, Billing & Support Section

AWS
Organization![](Pictures/10000001000002D5000001CBAFE06B0BEFCB4A8C.png){width="2.6244in"
height="1.6618in"}

-   Allows to manage multiple AWS accounts

-   The main account is the master account

-   Cost Benefits:

    -   Consolidated Billing across all accounts -- single payment
        method
    -   Pricing benefits from aggregated usage(volume discount for EC2,
        S3..)
    -   Pooling of Reserved EC2 instances for optimal savings

-   API is available to automate AWS account creation

-   Restric account privileges using Service Control Policies (SCP)

Multi Account Strategies

-   Create accounts per department, per cost center, per dev/test/prod,
    based on regulatory restricitions (using SCP), for better resources
    isolation (ex: VPC), to have separate per-account service limits,
    isolated account for logging
-   Multi Account vs One Account Multi VPC
-   Use tagging standards for billing purposes
-   Enable CloudTrail on all accounts, send logs to central S3 account
-   Send CloudWatch Logs to central logging account

Service Control Policies (SCP)

-   Whitelist or blacklist IAM actions

-   Applied at the OU(Organization Unit) or Account level

-   Does not apply to the Master Account

-   SCP is applied to all the Users and Roles of the account, including
    root user

-   The SCP does not affect service-linked roles

    -   Service-linked roles enable other AWS services to integrate with
        AWS Organizations and can't be restricted by SCPs

-   SCP must have an explicit Allow (does not allow anything by default)

-   Use cases:

    -   Restrict access to certain services (ex: can't use EMR)
    -   Enforce PCI compliance by explicitly disabling service

SCP Hirarchy

![](Pictures/1000000100000428000001B04B1CF599509F6524.png){width="6.9252in"
height="2.811in"}

AWS Organization -- Consolidated Billing

-   When enabled, provides you with:

    -   Combined Usage -- combine the usage across all AWS accounts in
        the AWS Organization to share the volume pricing, Reserved
        Instances and Savings Plans discounts
    -   One Bill -- get one bill for all AWS Accounts in the AWS
        Organization

-   The management account can turn off Reserved Instances discount
    sharing for any account in the AWS Organization, including itself

AWS Control Tower

-   Easy way to set up and govern a secure and compliant multi-account
    AWS environment based on best practicies

-   Benefits:

    -   Automate the set up of your environment in a few clicks
    -   Automate ongoing policy management using guardrails
    -   Detect policy violations and remediate them
    -   Monitor compliance through an interactive dashboard

-   AWS Control Tower runs on top of AWS Organizations:

    -   It automatically sets up AWS Organizations to organize accounts
        and implement SCPs

Pricing Models in AWS

-   AWS has 4 pricing models:

-   Pay as you go: pay for what you use, remain agile, responsive, meet
    scale demands

-   Save when you reserve: minimize risks, predictably manage budgets,
    comply with long-terms requirements

    -   Reservations are available for EC2 Reserved Instances, DynamoDB
        Reserved Capacity, ElastiCache Reserved Nodes, RDS Reserved
        Instance, Redshift Reserved Nodes

-   Pay less by using more: volume-based discounts

-   Pay less as AWS grows

Free services & free tier in AWS

-   IAM, VPC, Consolidated Billing

-   You do pay for the resources created: Elastic Beanstalk,
    CloudFormation, Auto Scaling Groups

-   Free Tier:

    -   EC2 t2.micro instance for a year
    -   S3, EBS, ELB, AWS Data transfer

AWS Compute Optimizer

-   Reduce costs and improve performance by recommending optimal AWS
    resources for your workloads
-   Helps you choose optimal configurations and right-size your
    workloads
-   Uses Machine Learning to analyze your resources' configurations and
    their utilization CloudWatch Metrics
-   Lower your costs by up to 25%
-   Recommendations can be exported to S3

Billing and Costing Tools

-   Estimating costs in the cloud:

    -   Pricing Calculatorimate the cost for your solution architecture

-   Tracking costs in the cloud:

    -   Billing Dashboard

    -   Cost Allocation Tags

        -   Use to track your AWS costs on a detailed level

        -   AWS generated tags

            -   Automatically applied to the resource you create
            -   starts with Prefix aws: (aws:createdBy)

        -   User defined tags

            -   defined by the user
            -   starts with Prefix user:

        -   Tags are used for organizing resources

        -   Tags can be used to create Resource Groups

    -   Cost and Usage Reports

        -   contains the most comprehensive set of AWS costs and usage
            data available, including additional metadata about AWS
            services, pricing, and reservations
        -   lists AWS usage for each service category by an account and
            its IAM users in hourly or daily line items, as well as any
            tags that you have activated for cost allocation purposes.

    -   Cost Explorer

        -   Visualize, understand, and manage your AWS costs and usage
            over time

        -   Create custom reports that analyze cost and usage data

        -   Analyze your data at a high level: total costs and usage
            across all accounts

        -   Choose an optimal Saving Plan

        -   Forecast usage up to 12 months based on previous usage

-   Monitoring against costs plans:

    -   Billing Alarms

        -   Billing data metric is stored in CloudWatch us-east-I
        -   It's for actual cost, not for projected costs

    -   Budgets

        -   Create budget and send alarms when costs exceeds the budget
        -   3 types of budgets: Usage, Cost, Reservation
        -   Up to 5 SNS notifications per budget

Trusted Advisor

-   No need to install anything -- high level AWS account assessment

-   Analyze your AWS accounts and provides recommendation on 5
    categories

    -   Cost optimization
    -   Performance
    -   Security
    -   Fault tolerance
    -   Service limits

Trusted Advisor -- Support Plans

![](Pictures/1000000100000411000001AAA629C80617CF2BD3.png){width="5.5618in"
height="2.2756in"}

AWS Support Plans Pricing

-   Basic Support: free

    -   Customer Service & Communities - 24x7 access to customer
        service, documentation, whitepapers, and support forums.
    -   AWS Trusted Advisor - Access to the 7 core Trusted Advisor
        checks and guidance to provision your resources following best
        practices to increase performance and improve security.
    -   AWS Personal Health Dashboard - A personalized view of the
        health of AWS services, and alerts when your resources are
        impacted.

-   Developer Support

    -   Business hours email access to Cloud Support Associates

    -   Unlimited cases / 1 primary contact

    -   Case severity / response times:

        -   General guidance: \< 24 business hours
        -   System impaired: \< 12 business hours

-   Business Support

    -   Intended to be used if you have production workloads

    -   Trusted Advisor -- Full set of checks + API access

    -   24x7 phone, email, and chat access to Cloud Support Engineers

    -   Unlimited cases / unlimited contacts

    -   Access to Infrastructure Event Management for additional fee

    -   Case severity / response times:

        -   Production system impaired: \< 4 hours
        -   Production system down: \< 1 hour

-   Enterprise On-Ramp Support

    -   Intended to be used if you have production or business critical
        workloads

    -   Access to a pool of Technical Account Managers (TAM)

    -   Concierge Support Team (for billing and account best practices)

    -   Infrastructure Event Management, Well-Architected & Operations
        Reviews

    -   Case severity / response times:

        -   Business-critical system down: \< 30 minutes

-   Enterprise Support Plan

    -   Intended to be used if you have mission critical workloads

    -   Access to a designated Technical Account Manager (TAM)

        -   Business-critical system down: \< 15 minutes

Account Best Practices -- Summary

-   Operate multiple accounts using Organizations
-   Use SCP (Service Control Policies) to restrict account power
-   Easily setup multiple accounts with best-practices with AWS Control
    Tower
-   Use Tags & Cost Allocation Tags for easy management & billing
-   IAM guidelines: MFA, least-privilege, password policy, password
    rotation
-   Config to record all resources configurations & compliance over time
-   CloudFormation to deploy stacks across accounts and regions
-   Trusted Advisor to get insights, Support Plan adapted to your needs
-   Send Service Logs and Access Logs to S3 or CloudWatch Logs
-   CloudTrail to record API calls made within your account
-   If you Account is compromised: change the root password, dlete and
    rotate all passwords/keys, contact the AWS support

Billing and Costing Tools -- Summary

-   Compute Optimizer: recommends resources' configurations to reduce
    cost
-   Pricing Calculator: cost of services on AWS
-   Billing Dashboard: high level overview + free tier dashboard
-   Cost Allocation Tags: tag resources to create detailed reports
-   Cost and Usage Reports: most comprehensive billing dataset
-   Cost Explorer: View current usage (detailed) and forecast usage
-   Billing Alarms: in us-east-I -- track overall and per-service
    billing
-   Budgets: more advanced -- track usage, costs, RI, and get alerts
-   Saving Plans: easy way to save based on long-term usage of AWS

AWS IAM Identity -- Summary

-   IAM

    -   For users that you trust and belong to your company

-   Organizations -- manage multiple AWS accounts

-   Secuirty Token Service(STS): temporary, limited-privileges
    credentials to access AWS resources

-   Cognito: create a database of users for your mobile & web
    applications

-   Directory Services: integrate Microsoft Active Directory in AWS

-   IAM Identity Center: one login for multiple AWS accounts &
    applications

Amazon WorkSpaces

-   Managed Desktop as a Service (DaaS) solution to easily provision
    Windows or Linux desktops
-   Great to eliminate management of on-premise VDI (Virtual Desktop
    Infrastracture)
-   Fast and quicky scalable to thousands of users
-   Secured data -- integrates with KMS
-   Pay-as-you-go service with montly or hourly rates

Amazon AppStream 2.0

-   Desktop Application Streaming Service
-   Deliver to any computer, without acquiring, provisioning
    infrastructure
-   The application is delivered from within a web browser

AppStream 2.0 vs WorkSpaces

-   WorkSpaces

    -   Fully managed VDI and desktop available
    -   The users connect to the VDI and open native or WAM applications
    -   Workspaces are on-demand or always on

-   AppStream 2.0

    -   Stream a desktop application to web browsers(no need to connect
        to a VDI)
    -   Works with any device(that has a web browser)
    -   Allow to configure an instance type per application type
        (CPU,RAM,GPU)

Amazon Sumerian

-   Create and run virtual reality(VR), augmented reality(AR), and 3D
    applications
-   Can be used to quickly created 3D models with animations
-   ready-to-use templates and assets -- no programming or 3D expertise
    required
-   Accessible via a web-browser URLs or on popular hardware for AR/VR

AWS IoT Core

-   IoT : Internet of Things: the network of internet-connected devices
    that are able to collect and transfer data
-   AWS IoT Core allows you to easily connect IoT devices to the AWS
    Cloud
-   Serverless, secure & scalable to billions of devices and trillions
    of messages
-   Your applications can communicate with your devices even when they
    aren't connected
-   Build IoT applications that gather, process, analyze, and act on
    data

Amazon Elastic Transcoder

-   Elastic Transcoder is used to convert media files stored in S3 into
    media files in the formats required by consumer playback devices(
    phones etc)

AWS AppSync

-   Store and sync data across mobile and web apps in real-time
-   Makes use of GraphQL (mobile technology from Facebook)
-   Client Code can be generated automatically
-   Offline data synchronization
-   AWS Amplify can leverage AWS AppSync in the background

AWS Amplify

-   A set of tools and services that helps you develop and deploy
    scalable full stack web and mobile applications
-   Authentication, Storage, API, CI/CD, PubSub, Analytics, AI/ML
    Predictions, monitoring, Source Code from AWS, GitHub...

AWS Device Farm

-   Fully-managed service that tests your web and mobile apps against
    desktop browsers, real mobile devices, and tablets
-   Run tests concurrently on multiple devices
-   Ability to configure settings (GPS, language, Wi-Fi, Bluetooth..)

AWS Backup

-   Fully-managed service to centrally manage and automate backups
    across AWS services
-   On-demand and scheduled backups
-   Support PITR(Point-in-time Recovery)
-   Retention Periods, Lifecycle Management, Backup Policies
-   Cross-Region Backup
-   Cross-Account Backup (using AWS Organizations)

Disaster Recovery Strategies

-   Backup and Restore : cheapest
-   Pilot Light: Core functions of the app Ready to scale, but minimal
    setup
-   Warm Standby: full version of the app, but at minimum size
-   Multi-Site/Hot-Site: full version of the app, at full size

AWS Elastic Disaster Recovery (DRS)

-   Used to be named "CloudEndure Disaster Recovery"
-   Quickly and easily recover your physical, virtual, and cloud-based
    servers into AWS
-   Example: protect your most critical databases (including Oracle,
    MySQL, and SQL Server), enterprise apps (SAP), protect your data
    from ransomware attacks, ...
-   Continuous block-level replication for your servers

AWS DataSync

-   Move large amount of data from on-premises to AWS
-   Can synchronize to: Amazon S3 (any storage classes -- including
    Glacier), Amazon EFS, Amazon FSx for Windows
-   Replication tasks can be scheduled hourly, daily, weekly
-   The replication tasks are incremental after the first full load

AWS Application Discovery Service

-   Plan migration projects by gathering information about on-premises
    data centers

-   Server utilization data and dependency mapping are important for
    migrations

-   Agentless Discovery (AWS Agentless Discovery Connector)

    -   VM inventory, configuration, and performance history such as
        CPU, memory, and disk usage

-   Agent-based Discovery (AWS Application Discovery Agent)

    -   System configuration, system performance, running processes, and
        details of the network connections between systems

-   Resulting data can be viewed within AWS Migration Hub

AWS Application Migration Service (MGN)

-   The "AWS evolution" of CloudEndure Migration, replacing AWS Server
    Migration Service (SMS)
-   Lift-and-shift (rehost) solution which simplify migrating
    applications to AWS
-   Converts your physical, virtual, and cloud-based servers to run
    natively on AWS
-   Supports wide range of platforms, Operating Systems, and databases
-   Minimal downtime, reduced costs

AWS Fault Injection Simulator (FIS)

-   A fully managed service for running fault injection experiments on
    AWS workloads
-   Based on Chaos Engineering -- stressing an application by creating
    disruptive events (e.g., sudden increase in CPU or memory),
    observing how the system responds, and implementing improvements
-   Helps you uncover hidden bugs and performance bottlenecks
-   Supports the following AWS services: EC2, ECS, EKS, RDS...
-   Use pre-built templates that generate the desired disruptions

AWS Step Functions

-   Build serverless visual workflow to orchestrate your Lambda
    functions
-   Features: sequence, parallel, conditions, timeouts, error handling,
    ...
-   Can integrate with EC2, ECS, On -premises servers, API Gateway, SQS
    queues, etc ...
-   Possibility of implementing human approval feature
-   Use cases: order fulfillment, data processing, web applications, any
    workflow

AWS Ground Station

-   Fully managed service that lets you control sattelite
    communications, process data, and scale your satellite operations
-   Provides a global network of satellite ground stations near AWS
    regions
-   Allows you to download satellite data to your AWS VPC within seconds
-   Send satellite data to S3 or EC2 instance
-   Use cases: weather forecasting, surface imaging, communications,
    video broadcasts

Amazon Pinpoint

-   Scalable 2-way (outbound/inbound) marketing communications service
-   Supports email, SMS, push, voice, and in-app messaging
-   Ability to segment and personalize messages with the right content
    to customers
-   Possibility to receive replies
-   Scales to billions of messages per day
-   Use cases: run campaigns by sending marketing, bulk, transactional
    SMS messages
-   Versus Amazon SNS or Amazon SES
-   In SNS & SES you managed each message\'s audience, content, and
    delivery schedule
-   In Amazon Pinpoint, you create message templates, delivery
    schedules, highly-targeted segments, and full campaigns

Well Architected Framework 6 Pillars

-   Operational Excellence
-   Security
-   Reliability: Ability of a system to recover from infrastructure or
    service disruptions, dynamically acquire computing resources to meet
    demand, and mitigate disruptions such as misconfigurations or
    transient network issues
-   Performance Efficiency
-   Cost Optimization
-   Sustainability

AWS Support

![](Pictures/10000001000002FA000001380F815451F0DC5A3E.png){width="6.3508in"
height="2.6in"}
