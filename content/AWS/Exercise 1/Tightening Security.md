+++
title = "Tightening Security"
weight = 1
draft = false
+++


With the application now running and all addresses known, there are various ways we can minimize vulnerabilities in the stack, specifically where network traffic can be allowed to flow. The following are areas that can be fixed:

On the PostgreSQL EC2 instance:

1. **/var/lib/pgsql/14/data/pg_hba.conf**
   - hosts allowed to connect can be set to be **only** the Spring Boot instance's private IP address

2. **EC2 Security Group**: kelly-postgresql
   - the rule for accepting traffic on port 5432 can be set to be only the Spring Boot instance's private IP address