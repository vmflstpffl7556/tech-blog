+++
title = "PostgreSQL EC2 Setup"
weight = 1
draft = false
+++
<!--when the draft = true => it wont' show on the website 
weight will decide the order of menu (the lower the first )-->


### Launch a new EC2 instance
- Provisioning a Linux-based virtual machine from AWS for use as a singular database node
- Allow the instance to be publicly connected to:
  - for administrative processes via SSH, port 22 over TCP
  - for database operations via PostgreSQL default port 5432 over TCP

1. Navigate to the EC2 dashboard in the AWS Management Console

2. Launch a new instance
    - **Names and tags**: kelly-g-autos-postgresql
    - Select Amazon Machine Image (AMI): **Amazon Linux 2**
      - Amazon Linux 2 is lightweight and comes prepackaged with the aws-cli
    - Select **instance type**: t2.micro (1 vRAM, 1 vCPU)
    - **Key pair**:
      - needed for admin access to the instance via SSH
      - authenticated with a cryptographic public/private key pair
      - Create new key pair or select a key pair for which you have already

    - **Network settings**:
        - Default VPC
        - Create a new **security group** named: kelly-g-autos-postgresql
          - Edit existing security group rule for SSH, port 22, select source as "Anywhere"
          - Add new security group rule:
            - Type: PostgreSQL
            - Protocol: TCP (Auto selected)
            - Port Range: 5432 (Auto selected)
            - Source type: Anywhere (we'll change this later)
      - Configure storage: use default values
      - Advanced details: use default values

3. Review your configuration summary and Launch instance.

{{% notice note %}}
How to create a new key pair<br>
*key pair type: ED25519 (Not supported for Windows instances)<br>
*Private key file format: .pem<br>
*copy downloaded pem file and past it in /home/user folder (ex:/home/vacpau)<br>
*chmod 400 kelly.pem
{{% /notice %}}

  
<br/>

### Connecting to the EC2 instance
1. Navigate to the EC2 instance dashboard

2. Select the instance to connect to, kelly-g-autos-postgresql
  
3. **Connect** and follow the directions very carefully in the **SSH Client** tab.
    - ```chmod 400 kelly-postgresql-keypair.pem```
    - run ssh command from where pem file is located
      ```ssh -i "kelly-postgresql-keypair.pem" ec2-user@ec2-54-210-159-129.compute-1.amazonaws.com```

4. Once you're connected, you should see your terminal prompt change:
      ``` Last login: Fri Oct 21 18:23:02 2022 from 76.147.216.35
              __|  __|_  )
              _|  (     /   Amazon Linux 2 AMI
              ___|\___|___|

          https://aws.amazon.com/amazon-linux-2/
          13 package(s) needed for security, out of 16 available
          Run "sudo yum update" to apply all updates.
          [ec2-user@ip-172-31-83-48 ~]$
      ```

5. Follow the terminal output's directions from EC2 and run system-wide package updates to the latest for the most secure and up-to-date tools.
   - ```sudo yum update -y```


<br/>

### Installing PostgreSQL 14 on Amazon Linux 2

1. After installing system-wide updates, use the following command to update the **yum** package manager's list of repositories for downloading packages. These lines will add the repos for PostgreSQL 14:
    ```
    sudo tee /etc/yum.repos.d/pgdg.repo<<EOF
    [pgdg14]
    name=PostgreSQL 14 for RHEL/CentOS 7 - x86_64
    baseurl=http://download.postgresql.org/pub/repos/yum/14/redhat/rhel-7-x86_64
    enabled=1
    gpgcheck=0
    EOF
    ```
2. Refresh the repository list

   ```sudo yum update -y```

3. Install PostgreSQL 14

   ```sudo yum install postgresql14 postgresql14-server -y```

4. Initialize PostgreSQL 14

    ```sudo /usr/pgsql-14/bin/postgresql-14-setup initdb```

<br/>

### Configure PostgreSQL 14

1. Configure the server to listen on all addresses (open public connections). This is not secure, but allows us to test connections before later locking down on these addresses:

    ```sudo nano /var/lib/pgsql/14/data/postgresql.conf```

    - Uncomment and set value for **listen_addresses = '*'**

    - Nano is a simple terminal text editor. It's easier to leave **nano** than **vi** or **vim**. Exit nano using **ctrl+X**, **Y**, and **Enter**.


2. Configure the server to accept HBA (host-based-authentication) from remote sources:

    ```sudo nano /var/lib/pgsql/14/data/pg_hba.conf```
    <br/>
    - Scroll to the very bottom and add a line to the IPv4 connections section so it looks like this: 
      
      ```
      IPv4 local connections:
      host    all             all             127.0.0.1/32     scram-sha-256
      host    postgres        autos           0.0.0.0/0        scram-sha-256
      ```

    - Exit **nano** using **crtl+X**, **Y**, and **Enter**.

3. Enable the server to start when the instance does:

    ```sudo systemctl enable --now postgresql-14```

4. Assume the postgres user that was automatically created upon installation:

    ```sudo su - postgres```

5. Start **psql** to connect to our PostgreSQL server. psql is a CLI client for PostgreSQL:

    ```psql```

6. Create the "autos" user

    ```create user autos with password 'autos123';```

7. Copy permissions on the database "postgres" to the user "autos".

    ```grant all on database postgres to autos;```

<br/>

### Review

At this point, you've successfully launched an EC2 instance and installed PostgreSQL to it and made it available to connect to with the proper credentials over the public internet! Not very safe, yet.

<br/>

### Testing the database connection remotely
Back on your local (or Cloud9) g-autos set up, we have the application set to connect to a local PostgreSQl server.

1. Copy the public IP address (public DNS works too) to your PostgreSQL instance. It can be found in the EC2 dashboard of the Management Console in the instance's details.

2. Change the following value in **/src/main/resources/appliation.properties**:

    ```DB_HOST=PUBLIC_IP_ADDRESS_TO_EC2_POSTGRESQL_SERVER```

3. Run the application with:

    ```./gradlew bootRun```

4. Upon success, you now have your local (or Cloud9) running Spring Boot app connected to a remote PostgreSQL server on an EC2 instance.
   
    ```http://localhost:8080/autos```

