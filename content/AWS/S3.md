+++
title = "S3"
weight = 2
draft = false
+++

### S3(Simple Storage Service)

- Bucket Policies
  - Bucket Policies are resource based, they are attached to buckets instead of Principals (Users, Groups, Roles)

<br>

#### Create an S3 Bucket

1. Navigate to the AWS S3 Dashboard, click Create Bucket

2. Enter a bucket name - Bucket names must be unique across all existing bucket names in Amazon S3. There are a number of other restrictions  on S3 bucket names as well.

3. Select a region to create your bucket in.

4. For this first bucket, we want **Object Ownership** and to disable ACLs (Access Control Lists for objects)

    - Object Ownership means that all of the objects created in this bucket will be owned by you, who created the bucket - that is because it is possible to allow other accounts to create objects in your bucket. This centralizes control of the objects in your bucket. ACLS (Access Control Lists) for objects can no longer have effect and you control ALL objects at the same time through a Bucket Policy - we'll get to that soon.

5. There are several other options you can enable for your S3 Bucket including

    - Block Public Access -
    - Versioning -
    - Tags -
    - Default Encryption -

   For now, leave them default and select Next.

6. Review your configuration settings and select, Create Bucket.

<br>


#### Upload a File

1. You will see your new bucket in the S3 console. Click on your bucket’s name to navigate to the bucket.

2. You are on your bucket’s home page. Select Upload.
3. To select a file to upload, either click Add files and select a sample file that you would like to store OR Drag and Drop a file on the upload box. Select Next after you have selected a file to upload.
4. You can set property settings like:
    - Storage class
    - Encryption
    - Tags
    - Metadata Leave the default values and select Next.
5. Review your configurations and select Upload. You will see your object in your bucket’s home screen.

<br>


#### Making your Object Publicly Accessible
1. At your bucket's home screen, click on your newly uploaded file's name

2. You should see all the details about your object. Under **Object URL** you'll find the address to your object, click it.

3. You should receive an error like this:
    ```
    <Error>
        <Code>AccessDenied</Code>
        <Message>Access Denied</Message>
        <RequestId>D9P6HGNGWNTP9J2Z</RequestId>
        <HostId>cxdNy+O58V5bYHTrA6+234g0GBlTrG2bCoXJlcEAb1DzrNzEGdMS+5J2l816dsPDBDWQe821l6o=</HostId>
    </Error>
    ```

4. To allow this object to be accessed publicly, we'll have to enable Public Access at the Bucket level. Navigate back to your bucket's details page.
5. At your bucket's details page, select the **Permissions** tab.
6. Under **Block Public Access**, select **Edit**
7. Uncheck **Block all public access** and **Save Changes**
8. Confirm
9. Select the **Objects** tab, navigate to your object and select it.
10. Click on your Object URL
    - We still get an **Access Denied** error. It seems that we've enabled public access to the objects in the bucket, however that does not retroactively and automatically enable public access for all objects inside the bucket. Let's make sure our object is accessible.
11. Because we disabled object ACLs, we cannot edit the public access for our object at the object level. Instead, we must allow access at the bucket level with a Bucket Policy. Navigate to the bucket's Permission tab
12. Edit the Bucket Policy (currently blank)
    - Use the right-hand panel to customize your placeholder policy. In the end, it should look similar to this. Make sure that your resource is named properly. The "*" is the wild-card symbol meaning "all".
    ```
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "public-read",
                "Effect": "Allow",
                "Principal": "*",
                "Action": [
                    "s3:GetObject",
                    "s3:GetObjectVersion"
                ],
                "Resource": "arn:aws:s3:::a-very-unique-bucket-name-01/*"
            }
        ]
    }
    ```

13. Save your policy and you should now be able to access your bucket's objects publicly and any new object created in it.

<br>


#### Allowing ACLs for Objects

If you want tighter control of the objects in your bucket, but have a bucket policy affecting all your objects in the bucket, you can enable ACLs when creating the bucket or enable them after bucket creation.

Through object ACLS, objects public accessibility can be controlled at the object level individually.

These ACLs can be found by navigating to the object's Permissions tab and Editing the Access Control List for that object. Just don't forget to disable Block Public Access at the bucket level too.