+++
title = "Project Environment"
weight = 1
draft = false
+++
<!--when the draft = true => it wont' show on the website 
weight will decide the order of menu (the lower the first )-->


### 1. Project Environment Setting
- Java11
- IDE: IntelliJ

### 2. Project Build with Spring Initizalizr
- https://start.spring.io
- Select Projects
  - Project: Gradle Project
  - Spring Boot: 2.3.x
  - Language: Java
  - Packaging: Jar
  - Java: 11
- Project Metadata
  - groupId: hello
  - artifactId: hello-spring
- Dependencies: Spring Web, Thymeleaf

![spring](../../../images/start-spring-io.png?width=800px&height=350px)
    
- Click Generate, and download zip file
- unzip and open project with IntelliJ

{{% notice note %}}
Gradle Global Setting: **build.gradle**
{{% /notice %}}

```
plugins {
	id 'org.springframework.boot' version '2.7.5'
	id 'io.spring.dependency-management' version '1.0.15.RELEASE'
	id 'java'
}

group = 'hello'
version = '0.0.1-SNAPSHOT'
sourceCompatibility = '11'

repositories {
	mavenCentral()
}

dependencies {
	implementation 'org.springframework.boot:spring-boot-starter-thymeleaf'
	implementation 'org.springframework.boot:spring-boot-starter-web'
	testImplementation 'org.springframework.boot:spring-boot-starter-test'
}

tasks.named('test') {
	useJUnitPlatform()
}
```

### 3. Testing
  - start main class
  - go to http://localhost:8080


{{% notice note %}}
Run Java directly instead of IntelliJ Gradle. The default setting for recent IntelliJ version is to run through Gradle. This will slow down execution. If you change it as follows, iti is executed directly in Java and the execution speed is faster.
Preferences Build, Execution, Deployment Build Tools Gradle
Build and run using: Gradle IntelliJ IDEA
Run tests using: Gradle IntelliJ IDEA
{{% /notice %}}

![intellij-grandle-settings](../../../images/intellij-grandle-settings.png?width=700px&height=350px)

### Gradle Library
> Gradle downloads dependent libraries together.
##### Spring Boot Library
  - spring-boot-starter-web
    - spring-boot-starter-tomcat: tomcat (web server)
    - spring-webmvc: spring web MVC
  - spring-boot-starter-thymeleaf: thymeleaf template engine(View)
  - spring-boot-starter: spring-bott + spring-core + logging
    - spring-boot
      - spring-core
    - spring-boot-starter-logging
      - logback, slf4j

##### Test Library
- spring-boot-starter-test
  - junit: test framework
  - mockito
  - assertj: A library that helps you write test code more conveniently
  - spring-test: Spring integration test support


### 4. View Environment
1. Welcome Page feature provided by Spring Boot. If static/index.html is uploaded, the Welcome page function is provided.
- resources/static/index.html
```html
<!DOCTYPE HTML>
<html>
<head>
    <title>Hello</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
Hello
<a href="/hello">hello</a>
</body>
</html>
```




Business Requirement
- data: userID, name
- feature: register, search
- db is not decided

- Controller: Web MVC controller
- Service: business logic
- Repository: database, saved in DB
- domain: business domain object
