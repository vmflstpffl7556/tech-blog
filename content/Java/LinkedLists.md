+++
title = "Linked Lists"
weight = 1
draft = false
+++

## LinkedList vs ArrayList
- Difference between ArrayList and LinkedList
  - Linked List: Dynamic in length, there is no index, spred out
  - Array List: fixed in length, contiguous

## LL: Constructor
```java
public class LinkedList{
    private Node head;
    private Node tail;
    private int length;

    class Node{
        int value;
        Node ntext;

        Node(int value){
            this.value = value;
        }
    }

    public LinedList(int value){
        Node newNode = new Node(value);
        head = newNode;
        tail = newNode;
        length = 1;
    }
}
```

