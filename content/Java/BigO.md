+++
title = "Big O"
weight = 1
draft = false
+++

## Big O
 - Big O is a way of comparing code one and code two mathematically about how efficient they run
 - Time Complexity
 - Space Complexity

## Big O: Worst Case
- Best Case: Omega
- Average Case: theta
- Worst Case: Omicron/O (Big O)

## Big O: O(n)

## Big O: Drop Constants
- two for loops
- n+n = o(n) (even though it's 2n, we just drop constants and o(n) not o(2n))

## Big O: O(n^2)
- for loop inside of for loop (nested for loops)
- n * n = n^2 = O(n^2) => less effecient from time complexity perspective

## Big O: Drop Non-Dominants
- nested for loops + for loop
- O(n^2) + O(n) = O(n^2 + n) = o(n^2) (drop the n)

## Big O: O(1)
- the number of operations stays constant
- most efficient Big O

## Big O: O(log n)
- log2-8 = 3 (2^3 = 8)
- sorted array (cut in half)
- very efficient after O(1)

## Big O: Different terms for inputs
- O(a)+O(b)=O(a+b)

## Big O: Array Lists
- looking with index: O(1)
- looking with value: O(n)

## Big O: Wrap up
- O(1) > O(log n) > O(n) > O(n^2)
- O(1): Constant
- O(log n): Devide and Conquer
- O(n): Proportional
- O(n^2): Loop within a Loop

