+++
title = "DOM"
weight = 4
draft = false
+++

### What is DOM and DOM Manuipulation?

#### What is the DOM
- DOM(Document Object Model) Structured representation of html documents. Allows javascript to access HTML elements and styles to manipulate them
- DOM is connection point between HTML documents and JavaScript code
- DOM is automatically created by the browser as soon as the HTML page loads
- Document: Special object that is the entry point to the DOM ex. document,querySelector();
- DOM !== JavaScript

##### Random number
```js
//1-20
let number = Math.trunc(Math.random()*20)+1;
//1-30
let number = Math.trunc(Math.random()*30)+1;
```

##### Replacing html value
```js
document.querySelector('.classname').testContent = "new Value";
```

##### Replacing input value
```js
document.querySelector('.classname').value = "new input value";
```

##### Replacing CSS value
```js
document.querySelector('body').style.backgroundColor='#60b347';

document.querySelector('.classname').style.width='30rem';
```

##### Handling Click Events
```js
document.querySelector('.classname').addEventListener('click',function(){
    console.log('You clicked!');
});
```

##### Modal
```js
const modal = document.querySelector('.modal');
const overlay = document.querySelector('.overlay');

//closing modal & overlay
const closModal = function(){
    //adding class
    modal.classList.add('hidden');
    overlay.classList.add('hidden');
}

//opening modal & overlay
const openModal = function(){
    //removing class
    modal.classList.remove('hidden');
    overlay.classList.remove('hidden');
}

//using key to close modal
document.addEventListener('keydown',function(e){
    console.log(e);
    if(e.key==="Escape" & !modal.classList.contains('hidden')){
        closeModal();
    }
})
```


##### Selecting Elements
```js
// 1. querySelector
document.querySelector('#id');
document.querySelector('.class');

// 2. getElementById
document.getElementById('id');
```

##### Adding/Removing class
```js
// add
document.querySelector('.className').classList.add('new-class-name');
// remove
document.querySelector('.className').classList.remove('new-class-name');
// toggle: if it exist, remove || if it does not exist, it add
document.querySelector('.className').toggle('new-class-name');
```

