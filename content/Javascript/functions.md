+++
title = "A Closer Look at Functions"
weight = 12
draft = false
+++

### Default Parameters

```js
const bookings = [];

// 2. ES6: How to set default value
const createBooking = function(flightNum, numPassengers =1 , price = 199){
  // 1. ES5: How to set default value
  // numPassengers = numPassengers || 1; //if the result is falsy value, use 1 
  // price = price || 199;

  const booking = {
    flightNum,
    numPassengers,
    price
  }
  console.log(booking);
  bookings.push(booking);
}

createBooking('LH123');//{flightNum: 'LH123', numPassengers: 1, price: 199}
createBooking('LH123',2,800);//{flightNum: 'LH123', numPassengers: 2, price: 800}
//how to use default value, set to undefined 
createBooking('LH123', undefined, 1000);//{flightNum: 'LH123', numPassengers: 1, price: 1000}
```

### How Passing Arguments Works: Values vs Reference
- Javascript does not have passing by reference, only passing by value
  ```js
    const flight = 'LH234';
    const kelly = {
    name: 'Kelly Lee',
    passport: 234564346
    }

    const checkIn = function(flightNum, passenger){
    flightNum = 'LH999';
    passenger.name = 'Mr. '+passenger.name;

    if(passenger.passport === 234564346 ){
        alert('Check in');
    }else{
        alert('Wrong passport!');
    }
    }

    checkIn(flight, kelly);//check in

    // 1. flight is string(primitive type)
    //flightNum is a copy and not the original value of the flight variable
    //it did not get reflected in the outside flight variable
    console.log(flight);//LH234

    // 2. kelly is object(reference type)
    //only copying the reference to that object in the memory heap, but they both poin to the same object in memory
    console.log(kelly);//{name: 'Mr. Kelly Lee', passport: 234564346}

    const newPassport = function(person){
    person.passport = Math.trunc(Math.random() * 1000000000);
    }

    newPassport(kelly);
    console.log(kelly);//{name: 'Mr. Kelly Lee', passport: 113518352}
    checkIn(flight,kelly);//Wrong Passport

  ```


### First-Class and Higher-Order Functions
- JS uses callbacks all the time
  ```js
    const oneWord = function(str){
    //remove all space
    return str.replace(/ /g, '').toLowerCase();
    }

    const upperFirstWord = function(str){
    const [first, ...others] = str.split(' ');
    return [first.toUpperCase(), ...others].join(' ');
    }

    //Higher-order function
    const transformer = function(str, fn){
    console.log(`Original String: ${str}`);
    console.log(`Transformed String: ${fn(str)}`);
    //fn.name : function name
    console.log(`Transformed by: ${fn.name}`);
    }

    //passing callback function upperFirstWord, not calling
    transformer('JavaScript is the best!', upperFirstWord);
    //Original String: JavaScript is the best!
    //Transformed String: JAVASCRIPT is the best!
    //Transformed by: upperFirstWord

    transformer(`JavaScript is the best!`, oneWord);
    //Original String: JavaScript is the best!
    //Transformed String: javascriptisthebest!
    //Transformed by: oneWord


    const high5 = function(){
    console.log('😃');
    }
    //addEventListner: higher-order function & high5: callback function
    document.body.addEventListener('click', high5);

  ```

### Functions Returning Functions

```js

const greet = function(greeting){
  return function(name){
    console.log(`${greeting} ${name}`);
  }
}

//save function into greeterHey variable
const greeterHey = greet(`Hey`);
console.log(greeterHey);
/*
ƒ (name){
  console.log(`${greeting} ${name}`);
}
*/
greeterHey('Kelly');//Hey Kelly

greet('Hello')('Kelly');//Hello Kelly

//convert to Arrow Function
const greetArr = greeting => name =>  console.log(`${greeting} ${name}`);
greetArr('Hi')('Kelly');//Hi Kelly
```

### The call, apply and bind Methods
- call(): manipulating this keyword, explicitly define the this keyword in any function
- apply(): apply does not receive a list of arguments after the this keyword, use array (not used in modern JavaScript)
- bind(): does not immediately call the function. instead it returns a new function where this keyword is bound. so it's set to whatever value wwe pass into bind
  
  ```js
  const lufthansa = {
    airline: 'Lufthansa',
    iataCode: 'LH',
    bookings: [],
    book(flightNum, name){
      console.log(`${name} booked a seat on ${this.airline} flight ${this.iataCode}${flightNum}`);
      this.bookings.push({flight: `${this.iataCode}${flightNum}`, name})
    }
  }

  lufthansa.book(239, 'Kelly Lee');//Kelly Lee booked a seat on Lufthansa flight LH239
  console.log(lufthansa);

  const eurowings = {
    airline: 'Eurowings',
    iataCode: 'EW',
    bookings: [],
  }

  //copy the method from lufthansa and save as book function
  const book =lufthansa.book;

  //this does not work => this keyword: undefined
  //book(23, 'Sarah Wiliams');//Uncaught TypeError: Cannot read properties of undefined (reading 'airline')

  //To fix, use call() method to set this keyword to eurowings
  book.call(eurowings, 23 ,'Sara Wiliams');//Sara Wiliams booked a seat on Eurowings flight EW23
  console.log(eurowings);

  //setting this keyword to luftansa
  book.call(lufthansa, 239, 'Mary Cooper');//Mary Cooper booked a seat on Lufthansa flight LH239
  console.log(lufthansa);

  const swiss = {
    airline: 'Swiss Air Line',
    iataCode: 'LX',
    bookings: [],
  }

  book.call(swiss, 583, 'Mary Cooper');//Mary Cooper booked a seat on Swiss Air Line flight LX583
  console.log(swiss);

  //apply() method => not used in modern JavaScript
  const flightData = [583, 'George Cooper'];
  book.apply(swiss, flightData);//George Cooper booked a seat on Swiss Air Line flight LX583
  console.log(swiss);

  //mordern JavaScript
  book.call(swiss, ...flightData);//George Cooper booked a seat on Swiss Air Line flight LX583

  //bind() method
  //create book function for every airline
  const bookEW = book.bind(eurowings);
  const bookLH = book.bind(lufthansa);
  const bookLX = book.bind(swiss);

  bookEW(23, `Steven Williams`);//Steven Williams booked a seat on Eurowings flight EW23

  //create book function for a specific flight
  //partial application: a part of the arguments of the original function are already set
  const bookEW23 = book.bind(eurowings, 23);
  bookEW23('Jonas Lee');//Jonas Lee booked a seat on Eurowings flight EW23


  //partial application
  const addTax = (rate, value) => value + value * rate;
  console.log(addTax(0.1, 200));//220

  //first arg is this keyword, but there is no this keyword in this function, just set to null
  //setting the preset rate to 0.23 using bind()
  const addVAT = addTax.bind(null, 0.23);
  console.log(addVAT(100));//123

  // another way to do the same thing
  const addTaxRate = function(rate){
    return function(value){
      return value + value * rate;
    }
  }
  const addVAT2 = addTaxRate(0.23);
  console.log(addVAT2(100));//123

  ```


### Immediately Invoked Function Expressions(IIFE)
- a function that disappears right after it's called once
```js
//transform to expression wrapping with () & at the end () => this will immediately call the function
(function(){
  console.log(`This will never run again`);
})();

//arrow function
(() => console.log('This will ALSO never run again'))();
```

### Closures
- Closure: variable envrionment attached to the function, exactly as it was at the time and place the function was created
- A Closure is the closed-over **variable environment** of the execution context **in which a function was created**, even **after** that execution context is gone.
- A closure gives a function access to all the variables **of its parent function**, even **after** that parent function has returned. The function keeps a **reference** to its outer scope, which **preserves** the scope chain throughout time.
- A closure makes sure that a function doesn't loose connection to **variables that existed at the function's birth place**
- A closure is like a **backpack** that a function carries around wherever it goes. This backpack has all the **variables that were present in the environment where the function was created**
- We do NOT have to manually create closures, this is a JavaScript feature that happens automatically. We can't even access closed-over variables explicitly. A closure is NOT a tangible JavaScript Object

- Example 1
- 
  ```js
  const secureBooking = function(){
    let passengerCount = 0;

    return function(){
      passengerCount++;
      console.log(`${passengerCount} passengers`);
    }
  }

  //booker is a function that was returned from secureBooking function
  const booker = secureBooking();

  booker();//1 passengers
  booker();//2 passengers

  //how does this work? how does booker function access the passengerCount variable?
  //any function always has access to the variable environment of the execution context in which the function was created.
  //in the case of booker, this function was created it was born in the execution context of secureBooking
  //therefore booker function will get access to this variable environment which contains the passengerCount variable
  //this connection is called closure

  console.dir(booker);
  /*
  [[Scopes]]: Scopes[3]
  0: Closure (secureBooking)
    passengerCount: 2
  */

  // [[]] double brackets means that it is an internal property which we cannot access from our code
  ```

- Example 2

  ```js
  let f;

  const g = function(){
    const a = 23;
    f = function(){
      console.log(a *2);
    }
  }

  const h = function(){
    const b = 777;
    f = function(){
      console.log(b*2);
    }
  }

  g();
  f();//46 => g() is no longer there, but still can use a variable
  console.dir(f);//[[Scopes]] 0: Closure (g) {a: 23}

  //Re-assinging f function
  h();
  f();//1554
  console.dir(f);//[[Scopes]] 0: Closure (h) {b: 777}

  ```


- Example 3
  
  ```js
  const boardPassengers = function(n, wait){
  const perGroup = n / 3;

  //use setTimeout(): first arg is function, second arg is milliseconds
  //function will be excuted in (wait*1) sec
  setTimeout(function(){
    console.log(`We are now boarding all ${n} passengers`);
    console.log(`There are 3 groups, each with ${perGroup} passengers`);
  }, wait * 1000)

  console.log(`Will start boarding in ${wait} seconds`);
  }
  //closure priority => this is not a priority, so this does not overwrite perGroup in boardPassengers
  const perGroup = 1000; 

  boardPassengers(180,3);
  //Will start boarding in 3 seconds
  //We are now boarding all 180 passengers
  //There are 3 groups, each with 60 passengers

  ```