+++
title = "Overview of Javascript"
weight = 2
draft = false
+++

### How JavaScript works Behind the Scenes!

<br>

##### 1. An High-Level Overview of JavaScript
- Low-Level: Developer has to manage resources manually (ex. C)
- **High-Level**: Developer does not have to worry, everything happens automatically(ex. **JavaScript**)

##### 2. Garbage-Collected
- cleaning the menory so we don't have to 

##### 3. Interpreted or just-in-time compiled

##### 4. Multi-paradigm
- Paradigm: An apprach and mindset of structuring code, which direct your coding style and technique.
  - Procedural programming
  - Object-oriented programming (OOP)
  - Functional programming (FP)
- **JavaScript does all of it**
  
##### 5. Prototype-based object-oriented

##### 6. First-class functions
- In a language with first-class functions, functions are simply treated as variables. We can pass them into other functions, and return them from functions.
  
##### 7. Dynamic 
- No data type definitions. Types becomes known at runtime
- Data type of variable is automatically changed
   
##### 8. Single-threaded

- Concurrency model: how the JavaScript engine handles multiple tasks happening at the same time
  <br>=> why do we need that?
  - JavaScript runs in one single thread, so it can only do one thing at a time.
  <br>=> so what about a long-running task?
    - Sounds like it would block the single thread. However, we want non-blocking behavior
  <br>=> How do we achieve that?
      - By using an event loop: takes long running tasks, executes them in the "background", and puts them back in the main thread once they are finished.
  
##### 9.  Non-blocking event loop

<br>

### JavaScript Engine
- JS Engine: program that executes javascript code (ex. V8 Engine)
  - **Call Stack**: Where our code is executed
  - **Heap**: Where objects are stored

<br>

### Computer Science SideNote: Compilation VS Interpretation
- Compilation: Entire code is converted into machine code at once, and written to a binary file that can be executed by a computer
- Interpretation: Interpreter runs through the source code and executes it line by line
- **JavaScript** used to be **interpretation** language. However, **modern JavaScript** is using **both** which is called **just-in-time compilation**
- **Just-in-time(JIT) comilation**: Entire code is converted into machine code at once, then executed immediately

