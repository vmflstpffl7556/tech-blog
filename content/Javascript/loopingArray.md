
+++
title = "Looping Array"
weight = 9
draft = false
+++

- Looping Array: The for-of Loop
- Enhanced Object Literals
- Optional Chaining(?.)
- Looping Objects: Object Keys, Values, and Entries

### Looping Array: The for-of Loop
```js
const restaurant = {
  starterMenu: ['Focaccia','Bruschetta','Bread','Salad'],
  mainMenu: ['Pizza','Pasta','Risotto'],
};

const menu = [...restaurant.starterMenu, ...restaurant.mainMenu];
console.log(menu); // ['Focaccia', 'Bruschetta', 'Bread', 'Salad', 'Pizza', 'Pasta', 'Risotto']

for(const item of menu) {
  console.log(item);
}
/*Focaccia
Bruschetta
Bread
Salad
Pizza
Pasta
Risotto*/

console.log(menu.entries()); //Array Iterator {}

console.log([...menu.entries()]);
/*
[0, 'Focaccia']
[1, 'Bruschetta']
[2, 'Bread']
[3, 'Salad']
[4, 'Pizza']
[5, 'Pasta']
[6, 'Risotto']
*/

for(const [i, el] of menu.entries()){
  console.log(`${i+1}: ${el}`);
}

/*
1: Focaccia
2: Bruschetta
3: Bread
4: Salad
5: Pizza
6: Pasta
7: Risotto
*/
```

<br>

### Enhanced Object Literals
```js
const weekdays = ['mon','tue','wed','thur','fri','sat','sun'];
const openingHours= {
  // 1. before ES6
  // thur:{
  //     open:12,
  //     close:22
  // },

  //2. Enhanced Object Literals
  [weekdays[3]]:{
    open:12,
    close:22
  },
  fri:{
      open:11,
      close:23
  },
  sat:{
      open: 0,
      close:24
  }
};

const restaurant = {
  starterMenu: ['Focaccia','Bruschetta','Bread','Salad'],
  mainMenu: ['Pizza','Pasta','Risotto'],
  //1. before ES6
  //openingHours: openingHours,

  //2. ES6 enhanced object literals
  openingHours,
  
  //1. before ES6
  // order: function(staterIndex, mainIndex){
  //   return [this.starterMenu[starterIndex], this.mainMenu[mainIndex]]
  // },

  //2. ES6 enhanced object literals => sligtly easier syntax 
  order(staterIndex, mainIndex){
    return [this.starterMenu[starterIndex], this.mainMenu[mainIndex]]
  }
};
```

<br>

### Optional Chaining(?.)
- Example 1
  ```js
  const openingHours= {
    thur:{
        open:12,
        close:22
    },
    fri:{
        open:11,
        close:23
    },
    sat:{
        open: 0,
        close:24
    }
  };

  const restaurant = {
    openingHours,
  };


  console.log(restaurant.openingHours.mon); //undefined
  // Issue
  //console.log(restaurant.openingHours.mon.open); //uncaught TypeError: cannot read property 'open' of undefined

  // Option1. old way
  // if openingHours and mon exist, then print
  if(restaurant.openingHours && restaurant.openingHours.mon){ 
    console.log(restaurant.openingHours.mon.open); //undefined
  }

  // Option 2. With optional chaining(?.)
  // only if mon exists, then open property will be read from there. but if not, return undefined
  console.log(restaurant.openingHours.mon?.open); //undefined
  //check if openingHours exist, check mon exist
  console.log(restaurant.openingHours?.mon?.open); //undefined

  ```

- Example 2
  ```js

    const openingHours= {
    thur:{
        open:12,
        close:22
    },
    fri:{
        open:11,
        close:23
    },
    sat:{
        open: 0,
        close:24
    }
    };

    const restaurant = {
    openingHours,
    };

    const weekdays = ['mon','tue','wed','thur','fri','sat','sun'];

    for(const day of weekdays){
    //console.log(day);
    const open = restaurant.openingHours[day]?.open;
    console.log(`On ${day}, we open at ${open}`);
    }
    /*
    On mon, we open at undefined
    On tue, we open at undefined
    On wed, we open at undefined
    On thur, we open at 12
    On fri, we open at 11
    On sat, we open at 0
    On sun, we open at undefined
    */


    //Setting default value
    //There is issue for Sat because of 0
    for(const day of weekdays){
    //console.log(day);
    const open = restaurant.openingHours[day]?.open || `closed`;
    console.log(`On ${day}, we open at ${open}`);
    }
    /*
    On mon, we open at closed
    On tue, we open at closed
    On wed, we open at closed
    On thur, we open at 12
    On fri, we open at 11
    On sat, we open at closed 
    On sun, we open at closed
    */

    //Solution => use the nullish coalescing operator
    for(const day of weekdays){
    //console.log(day);
    const open = restaurant.openingHours[day]?.open ?? `closed`;
    console.log(`On ${day}, we open at ${open}`);
    }
    /*
    On mon, we open at closed
    On tue, we open at closed
    On wed, we open at closed
    On thur, we open at 12
    On fri, we open at 11
    On sat, we open at 0 
    On sun, we open at closed
    */

  ```

- Example 3: methods
  ```js
  const restaurant = {
  starterMenu: ['Focaccia','Bruschetta','Bread','Salad'],
  mainMenu: ['Pizza','Pasta','Risotto'],
  order(staterIndex, mainIndex){
      return [this.starterMenu[staterIndex], this.mainMenu[mainIndex]]
  }
  };

  //methods => if method exist, call the method, also using nullish coalescing operator
  console.log(restaurant.order?.(0,1) ?? `Method does not exist`); //['Focaccia', 'Pasta'] 
  console.log(restaurant.order2?.(0,1) ?? `Method does not exist`); //Method does not exist
  ```

- Example 4: Arrays
  ```js
  const user = [
    {
      name: 'Jonas',
      email: 'hello@jonas.io'
    }
  ];

  const user2 = [];

  // 1. without optional chaining
  if(user.length>0){
    console.log(user[0].name);
  }else{
    console.log(`user array empty`);
  }

  // 2. with optional chaining
  // if user[0] exists && name exists
  console.log(user[0]?.name ?? `User array empty`); //Jonas
  console.log(user2[0]?.name ?? `User array empty`); //User array empty
  ```
<br>

### Looping Objects: Object Keys, Values, and Entries
- Object Keys: Object.keys();
- Object Values: Object.values();
- Object Entries: Object.entries();
  
```js
const openingHours= {
  thur:{
      open:12,
      close:22
  },
  fri:{
      open:11,
      close:23
  },
  sat:{
      open: 0,
      close:24
  }
};

//1. Property NAMES => Object.keys()
const properties = Object.keys(openingHours); 
console.log(properties); //['thur', 'fri', 'sat']
console.log(`We are open on ${properties.length} days`); //We are open on 3 days

let openStr = `We are open on ${properties.length} days: `;

for (const day of properties){
  openStr += `${day}, `;
}
console.log(openStr);// We are open on 3 days: thur, fri, sat, 

//2. Property VALUES => Object.values()
const values = Object.values(openingHours);
//console.log(values);

//3. Entire object => Object.entries();
const entries = Object.entries(openingHours);
console.log(entries);
/*
 ['thur', {open: 12, close: 22}]
 ['fri', {open: 11, close: 23}]
 ['sat', {open: 0, close: 24}]
*/

//value itself is an object here : Value={open,close}
for(const [key, {open, close}] of entries){
  console.log(`On ${key} we open at ${open} and close at ${close}`);
}
/*
On thur we open at 12 and close at 22
On fri we open at 11 and close at 23
On sat we open at 0 and close at 24
*/
```
