+++
title = "Working with Strings"
weight = 11
draft = false
+++


Working with Strings
- Example 1 : indexOf(), lastIndexOf(), slice()
  ```js
    const airline = `TAP Air Portugal`;
    const plane = `A320`;

    console.log(plane[0]);//A
    console.log(plane[1]);//3
    console.log(plane[2]);//2
    console.log(plane[3]);//0
    console.log('B737'[0]);//B

    console.log(airline.length)//16;
    console.log('B737'.length);//4

    //indexOf & lastIndexOf
    console.log(airline.indexOf('r'));//6
    console.log(airline.lastIndexOf('r'));//10
    console.log(airline.indexOf('Portugal'));//8
    console.log(airline.indexOf('portugal'));//-1 (case sensitive)

    //slice method : extract substring
    console.log(airline.slice(4));//Air Portugal
    console.log(airline.slice(4,7));//Air
    console.log(airline.slice(0, airline.indexOf(' ')));//TAP

    console.log(airline.slice(0, airline.lastIndexOf(' ')));//TAP Air
    console.log(airline.slice(airline.lastIndexOf(' ')));//  Portugal
    console.log(airline.slice(airline.lastIndexOf(' ')+1));//Portugal

    console.log(airline.slice(-2));//al
    console.log(airline.slice(1,-1));//AP Air Portuga

    //Example
    const checkMiddleSeat = function(seat){
    //B and E are middle seats
    const s = seat.slice(-1);
    if(s==='B' || s==='E')
    console.log('You got the middle seat!');
    else console.log('You got lucky!');
    }

    checkMiddleSeat('11B');//You got the middle seat!
    checkMiddleSeat('23C');//You got lucky!
    checkMiddleSeat('3E');//You got the middle seat!
  ```
- Example 2: toLowerCase(), toUppderCase(), trim(), trimStart(), trimEnd(), replace(), replaceAll(), includes()
  ```js
    const airline = `TAP Air Portugal`;

    console.log(airline.toLowerCase());//tap air portugal
    console.log(airline.toUpperCase());//TAP AIR PORTUGAL

    //1. Fix capitalization in name from jOnAS to Jonas
    const passenger = 'jOnAS';
    const passengerLower = passenger.toLowerCase();
    const passengerCorrect = passengerLower[0].toUpperCase()+passengerLower.slice(1);
    console.log(passengerCorrect);//Jonas

    //2. Check email
    const email = 'hello@jonas.io';
    const loginEmail = '  Hello@Jonas.Io \n';
    //2-1. way1
    const lowerEmail = loginEmail.toLowerCase();
    const trimmedEmail = lowerEmail.trim();
    console.log(trimmedEmail);//hello@jonas.io
    //2-2 way2
    const normalizedEmail = loginEmail.toLowerCase().trim();
    console.log(normalizedEmail);//hello@jonas.io

    console.log(email === normalizedEmail);//true

    // ES6 new Function: trimEnd(), trimStart()
    const test = " newFunction ";
    console.log(test.trimEnd());// newFunction
    console.log(test.trimStart());//newFunction

    //replace()
    const priceGB = `288,97£`
    const priceUS = priceGB.replace('£','$').replace(',','.');
    console.log(priceUS);//288.97$

    const announcement = 'All passengers come to boarding door 23. Boarding door 23!';
    //reaplce(): it only replace the first one.
    console.log(announcement.replace('door','gate'));//All passengers come to boarding gate 23. Boarding door 23!
    //replaceAll(): replace everything
    console.log(announcement.replaceAll('door','gate'));//All passengers come to boarding gate 23. Boarding gate 23!
    //using regular expression (g: global)
    console.log(announcement.replace(/door/g,'gate'));//All passengers come to boarding gate 23. Boarding gate 23!

    //Booleans
    const plane = 'Airbus A320neo';
    console.log(plane.includes('A320'));//true
    console.log(plane.includes('Boeing'));//false
    console.log(plane.startsWith('Ai'));//true
    console.log(plane.endsWith('o'));//true

    //exercise
    const checkBaggauge = function(item){
    const baggage = item.toLowerCase();
    if(baggage.includes('knife') || baggage.includes('gun')){
        console.log('You are not allowed on board');
    }else{
        console.log('Welcome aboard!')
    }
    }
    checkBaggauge('I have a laptop, some food and a pocket knife');//You are not allowed on board
    checkBaggauge('Socks and Cameron');//Welcome aboard!
    checkBaggauge('Got some snacks and a gun for protection');//You are not allowed on board
  ```

- Example 3: split(), join(), padStart(), padEnd(), repeat()
  ```js
    //split()
    console.log('a+very+nice+string'.split('+'));//["a","very","nice","string"]
    console.log('Kelly Lee'.split(' '));//["Kelly","Lee"]

    const [firstName, lastName] = 'Kelly Lee'.split(' ');
    console.log(firstName);//Kelly

    //join()
    const newName = ['Mr.', firstName, lastName.toUpperCase()].join(' ');
    console.log(newName);//Mr. Kelly LEE

    //exercise
    const capitalizeName = function(name){
      const names = name.split(' ');
      console.log(names);//['jessica', 'ann', 'smith', 'davis']

      //create new array
      const namesUpper = [];

      for(const n of names){
        //Optioin 1
        //namesUpper.push(n[0].toUpperCase()+n.slice(1));
        //Option 2
        namesUpper.push(n.replace(n[0],n[0].toUpperCase()));

      }
      console.log(namesUpper.join(' '));//Jessica Ann Smith Davis
    }
    capitalizeName('jessica ann smith davis');

    //padding a string: add a number of characters to the string until the string has a certain desired length
    const message = 'Go to gate 23!';

    // padStart()
    console.log(message.padStart(25,'+'));//+++++++++++Go to gate 23! (entire legnth is 25)
    console.log('Kelly'.padStart(25,'+'));//++++++++++++++++++++Kelly

    // padEnd()
    console.log(message.padStart(25,'+').padEnd(30,'+'));//+++++++++++Go to gate 23!+++++
    console.log('Kelly'.padStart(25,'+').padEnd(30,'+'));//++++++++++++++++++++Kelly+++++

    //exercise
    const maskCreditCard = function(number){
        const str = number+''; //change to string
        const last = str.slice(-4);//get the last four digits
        return last.padStart(str.length, '*');
    }
    console.log(maskCreditCard(123456789));//*****6789
    console.log(maskCreditCard('123456789123456789'));//**************6789

    //repeat()
    const message2 = 'Bad weather... All Departures Delayed... ';
    console.log(message2.repeat(3))//Bad weather... All Departures Delayed... Bad weather... All Departures Delayed... Bad weather... All Departures Delayed... 

    const planesInLine = function(n){
        console.log(`There are ${n} planes in line ${'✈️'.repeat(n)}`);
    }

    planesInLine(5);//There are 5 planes in line ✈️✈️✈️✈️✈️
  ```

String Methods Practice
```js
// String Methods Practice

const flights =
  '_Delayed_Departure;fao93766109;txl2133758440;11:25+_Arrival;bru0943384722;fao93766109;11:45+_Delayed_Arrival;hel7439299980;fao93766109;12:05+_Departure;fao93766109;lis2323639855;12:30';

// Result
// 🔴 Delayed Departure from FAO to TXL (11h25)
//              Arrival from BRU to FAO (11h45)
//   🔴 Delayed Arrival from HEL to FAO (12h05)
//            Departure from FAO to LIS (12h30)

const getCode = str => str.slice(0,3).toUpperCase();

for(const flight of flights.split('+')){
  const [type,from,to,time] = flight.split(';');
  const output =`${type.startsWith('_Delayed') ? '🔴': ''}${type.replaceAll('_',' ')} from ${getCode(from)} to ${getCode(to)} (${time.replace(':','h')})`.padStart(50);
  console.log(output);
}
```
