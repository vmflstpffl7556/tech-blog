+++
title = "Data Structures"
weight = 7
draft = false
+++

#### Destructuring Arrays

- Example 1
    ```js
    //destructuring
    const arr = [2,3,4];
    const [x,y,z] = arr;
    console.log(x,y,z); // 2,3,4 // this is not array
    console.log(arr); // [2, 3, 4] // this is array
    ```

- Example 2: Swtiching variable using destructuring
    ```js
    const restaurant = {
      categories: ['Italina','Pizzeria','Vegetrian','Organic']
    };

    const [first, ,second] = restaurant.categories;
    console.log(first,second);// Italian, Vegetrian

    let [main, , secondary] = restaurant.categories;
    console.log(main, secondary);// Italian, Vegetrian

    //Swtiching variable using destructuring
    [main,secondary] = [secondary,main];
    console.log(main, secondary);// Vegetrian Italina
    ```

- Example 3
    ```js
    const restaurant = {
      staterMenu: ['Focaccia','Bruschetta','Bread','Salad'],
      mainMenu: ['Pizza','Pasta','Risotto'],
      order: function(starterIndex, mainIndex){
        return [this.staterMenu[starterIndex], this.mainMenu[mainIndex]];
      }
    };

    console.log(restaurant.order(2,0)); //['Bread', 'Pizza']

    //destructuring
    //Receive 2 return values from a function
    const [starter,main] = restaurant.order(2,0);
    console.log(starter,main); //Bread Pizza

    ```

- Example 4: nested destructuring(nested: array inside of array)
    ```js
    const nested = [2,4,[5,6]];
    const [i, , j] = nested;
    console.log(i,j);//2 [5, 6]

    const [a, , [b,c]] = nested;
    console.log(a,b,c); // 2 5 6
    ```

- Example 5: setting default value
    ```js
    const [p,q,r] = [8,9];
    console.log(p,q,r); // 8 9 undefined

    // Setting default values
    const [a=1, b=2, c=3] = [8,9];
    console.log(a,b,c) //8 9 3
    ```
    <br>

#### Destructuring Objects
- Example1: destructing objects
  ```js
    const restaurant = {
      name: 'Classico',
      categories: ['Italina','Pizzeria','Vegetrian','Organic'],
      starterMenu: ['Focaccia','Bruschetta','Bread','Salad'],
    };

    //destructuring objects
    const {name, openingHours, categories} = restaurant;
    console.log(name, openingHours, categories); //Classico undefined ['Italina', 'Pizzeria', 'Vegetrian', 'Organic']

    //you can rename
    const{
      name: restaurantName, 
      openingHours: hours, 
      categories: tags
    } = restaurant;
    console.log(restaurantName,hours,tags); //Classico undefined ['Italina', 'Pizzeria', 'Vegetrian', 'Organic']

    //setting default value
    const{
      menu = ['default'], 
      starterMenu: starters = []
    } = restaurant;
    console.log(menu, starters); // ['default'] ['Focaccia', 'Bruschetta', 'Bread', 'Salad']
  ```

- Example 2: mutating variables
    ```js
    //mutating variables
    let a =111;
    let b =999;
    const obj = {a:23, b:7, c:14};

    //destructuring
    //{a,b}=obj; //error because { is considered as block
    ({a,b} = obj);
    console.log(a,b);//23 7
    ```

- Examlple 3: nested objects
    ```js
    const restaurant = {
      openingHours:{
        thur:{
          open:12,
          close:22
        },
        fri:{
          open:11,
          close:23
        },
        sat:{
          open: 0,
          close:24
        }
      },
    };

    //destructuring objects
    const {openingHours} = restaurant;
    console.log(openingHours); //{thur: {…}, fri: {…}, sat: {…}} 

    const{fri} = openingHours;
    console.log(fri); //{open: 11, close: 23}

    const {fri: {open, close}} = openingHours;
    console.log(open,close);//11 23
    ```

- Example 4: passing object to function
  ```js
    const restaurant = {
      starterMenu: ['Focaccia','Bruschetta','Bread','Salad'],
      mainMenu: ['Pizza','Pasta','Risotto'],

      // passing one object {obj}
      orderDelivery: function({starterIndex, mainIndex, time, address}){
        console.log(`Oder received! ${this.starterMenu[starterIndex]} and ${this.mainMenu[mainIndex]} will be delivered to ${address} at ${time}`);
      }
    };

    restaurant.orderDelivery({
        time: '22:30',
        address: 'Via del Sole, 21',
        mainIndex: 2,
        starterIndex: 2,
    });//Oder received! Bread and Risotto will be delivered to Via del Sole, 21 at 22:30
  ```

- Example 5: passing object to function with default values
  ```js
  const restaurant = {
    starterMenu: ['Focaccia','Bruschetta','Bread','Salad'],
    mainMenu: ['Pizza','Pasta','Risotto'],

    // passing one object {obj} && seeting default
    orderDelivery: function({starterIndex=1, mainIndex=0, time='20:00', address}){
      console.log(`Oder received! ${this.starterMenu[starterIndex]} and ${this.mainMenu[mainIndex]} will be delivered to ${address} at ${time}`);
    }
  };

  restaurant.orderDelivery({
    time: '22:30',
    address: 'Via del Sole, 21',
    mainIndex: 2,
    starterIndex: 2,
  });//Oder received! Bread and Risotto will be delivered to Via del Sole, 21 at 22:30

  restaurant.orderDelivery({
    address: 'Via del Sole, 21',
    starterIndex: 2,
  });//Oder received! Bread and Pizza will be delivered to Via del Sole, 21 at 20:00
  ```

The Spread Operator ...
  ```js
    //without using spread operator
    const arr = [7,8,9];
    const badNewArr = [1,2,arr[0],arr[1],arr[2]];
    console.log(badNewArr);//[1, 2, 7, 8, 9]

    //using spread operator 
    const newArr = [1,2, ...arr];
    console.log(newArr);//[1, 2, 7, 8, 9]

    //log individual element of array
    console.log(...newArr);// 1 2 7 8 9
 ```
 - use cases
  ```js
    const restaurant = {
        starterMenu: ['Focaccia','Bruschetta','Bread','Salad'],
        mainMenu: ['Pizza','Pasta','Risotto'],
    };

    //writing new array with spread operator
    const newMenu = [...restaurant.mainMenu, 'Gnocci'];
    console.log(newMenu);//['Pizza', 'Pasta', 'Risotto', 'Gnocci']

    //copy array
    const mainMenuCopy = [...restaurant.mainMenu];
    console.log(mainMenuCopy); //['Pizza', 'Pasta', 'Risotto']

    //join 2 ararys
    const menu = [...restaurant.mainMenu, ...restaurant.starterMenu];
    console.log(menu);//['Pizza', 'Pasta', 'Risotto', 'Focaccia', 'Bruschetta', 'Bread', 'Salad']

  ```
  - spread operator works on iterables(arrays, strings, maps, sets, NOT objects)
    ```js
        //strings
        const str = 'Kelly';
        const letters = [...str, '', 'S.'];
        console.log(letters);//['K', 'e', 'l', 'l', 'y', '', 'S.']
        console.log(...str);//K e l l y
    ```
    ```js
    //example
    const restaurant = {
        orderPasta: function(ing1,ing2,ing3){
            console.log(`Here is your delicious pasta with ${ing1}, ${ing2}, and ${ing3}`);
        }
    };

    const ingredients = [prompt("Let's make pasts! Ingredient1?"), prompt("Ingredient2?"), prompt("Ingredient3?")];

    console.log(ingredients);//['a', 'b', 'c']
    
    //without using spread operator
    restaurant.orderPasta(ingredients[0],ingredients[1],ingredients[2]);//Here is your delicious pasta with a, b, and c

    //using spread operator
    restaurant.orderPasta(...ingredients);//Here is your delicious pasta with a, b, and c
    ```

Rest Pattern and Parameters
- Rest pattern: collects the elements that are unused in the destructuring assignment
  ```js
    //Spread operator, because on Right side of operator(=)
    const arr = [1,2, ...[3,4]];
    console.log(arr);//[1, 2, 3, 4]

    //Rest pattern, because on LEFT side of operator(=)
    const [a,b, ...others] = [1,2,3,4,5];
    console.log(a,b,others);//1 2 [3, 4, 5]
    ```

- example
  ```js
    const restaurant = {
        starterMenu: ['Focaccia','Bruschetta','Bread','Salad'],
        mainMenu: ['Pizza','Pasta','Risotto'],
    }

    //using spread & rest
    const [pizza, , risotto, ...otherFood]=[...restaurant.mainMenu, ...restaurant.starterMenu];

    // it does not include any skipped element like "Pasta"
    console.log(pizza, risotto, otherFood); //Pizza Risotto ['Focaccia', 'Bruschetta', 'Bread', 'Salad']
    ```

    ```js
    //Object
    const {sat, ...weekdays} = restaurant.openingHours;
    console.log(weekdays);// {thur: {…}, fri: {…}}}
    ```
    ```js
    //Functions
    const add = function(...numbers){
        console.log(numbers);
        let sum =0;
        for(let i=0; i<numbers.length;i++) sum += numbers[i];
        console.log(sum)
    }
    add(2,3);//[2, 3] //5
    add(5,3,7,2);// [5, 3, 7, 2] //17
    add(8,2,5,3,2,1,4);//[8, 2, 5, 3, 2, 1, 4] //25

    const x = [23,5,7];
    add(...x);//[23, 5, 7] //35
    ```
    ```js
    //example
    const restaurant = {
        orderPizza: function(mainIngredient,...otherIngredients){
            console.log(mainIngredient); //mushrooms
            console.log(otherIngredients);// ['onion', 'olives', 'spinach']
        }
    };
    restaurant.orderPizza('mushrooms','onion','olives','spinach');
    ```

Short Circuiting (&& and ||)
- || short circuiting: it will return the first truthy value of all the operands, or simply the last value if all of them are falsy
    ```js
    //Use Any data type, return Any data type, short-circuting
    console.log( 3 || 'Kelly'); //3
    console.log('' || 'Kelly');//Kelly => '' is a falsy value
    console.log(true || 0);//true
    console.log(undefined || null); //null => both undefined and null are falsy values
    console.log(undefined || 0 || '' || 'Hello' || 23 );//'Hello'

    //example
    //option1. using turnery operator
    const guests1 = restaurant.numGuests ? restaurant.numGuests :10;
    console.log(guests1);//10
    //option2. using short circuiting
    const guests2 = restaurant.numGuests || 10;
    console.log(guests2);
    ```
- && short circuting: it will return the first falsy value of all the operands, or the last value if all of them are truthy
  ```js

    console.log(0 && 'Kelly');//0
    console.log(7 && 'Kelly');//Kelly
    console.log('Hello' && 23 && null && 'Kelly');//null

    //option1. use if statement
    //if orderPizza function exists, run the function
    if(restaurant.orderPizza){
        restaurant.orderPizza('mushrooms','spinach');
    }
    
    //option2. use && short cuircuiting
    //if orderPizza function does not exist(falsy) then nothing happens
    restaurant.orderPizza && restaurant.orderPizza('mushrooms','spinach');

  ```

Nullish Coalescing Operator (??)
- Nullish values: **null** and **undefined** (NOT 0 or '')
- return the first non-nullish value
  ```js
    restaurant.numGuests=0; //0 is a falsy value
    //using || cuircuiting operator
    const guests2 = restaurant.numGuests || 10;
    console.log(guests2); //10, but we want 0 (because 0 is considered a s afalsy value);

    // To fix it, use nullish coalescing operator
    const guestCorrect = restaurant.numGuests ?? 10 ;
    console.log(guestCorrect);//0, 0 is not considered as nullish value, so first non-nullish value is returned

  ```

Logical Assignment Operators

    ```js

    const rest1 = {
        name: 'Capri',
        numGuests: 20,
    };
    const rest2 = {
        name: 'La Pizza',
        owner: 'Giovanni Rossi',
    };

    //Option1. using || circuting operator
    rest1.numGuests = rest1.numGuests || 10;
    rest2.numGuests = rest2.numGuests || 10; //rest2.numGuests is undefined, therefore pick 10

    console.log(rest1.numGuests);//20
    console.log(rest2.numGuests);//10

    //Option2. using assignment operator
    rest1.numGuests ||= 10;
    rest2.numGuests ||= 10;

    console.log(rest1.numGuests);//20
    console.log(rest2.numGuests);//10
    ```
 - Nullish Assignment Operator (null or undefined)
    ```js
    const rest1 = {
        name: 'Capri',
        numGuests: 0,
    };
    const rest2 = {
        name: 'La Pizza',
        owner: 'Giovanni Rossi',
    };

    //Issue. using assignment operator
    // rest1.numGuests ||= 10; //rest1.numGuests is considered as falsy (0)
    // rest2.numGuests ||= 10;

    // console.log(rest1.numGuests);//10 => we want it to be 0
    // console.log(rest2.numGuests);//10

    //How to solve this issue: logical nullish assignment operator
    rest1.numGuests ??= 10; //rest1.numGuests is 0 => not nullish value
    rest2.numGuests ??= 10; //rest2.numGuests is undefined => nulish value
    console.log(rest1.numGuests);//0
    console.log(rest2.numGuests);//10
    ```
  - && assignment Operator
    ```js
    const rest1 = {
        name: 'Capri',
        numGuests: 0,
    };
    const rest2 = {
        name: 'La Pizza',
        owner: 'Giovanni Rossi',
    };

    //Option1.
    // rest1.owner = rest1.owner && '<ANONYMOUS>';
    // rest2.owner = rest2.owner && '<ANONYMOUS>';
    // console.log(rest1.owner);//undefined
    // console.log(rest2.owner);//<ANONYMOUS>

    //Option2. using && assignement operator
    rest1.owner &&= '<ANONYMOUS>';
    rest2.owner &&= '<ANONYMOUS>';
    console.log(rest1.owner);//undefined
    console.log(rest2.owner);//<ANONYMOUS>
    ```
