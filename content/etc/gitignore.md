+++
title = ".gitignore"
weight = 1
draft = false
+++
### .gitignore
- A gitignore file specifies intentionally untracked files that Git should ignore. Files already tracked by Git are not affected.

### Terraform
- [Github's Terraform .gitignore](https://github.com/github/gitignore/blob/main/Terraform.gitignore)