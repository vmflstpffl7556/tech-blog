+++
title = "Terraform Deployment"
weight = 4
draft = false
+++

- create S3 bucket to save terraform.state file

### Database main.tf

- create RDS DB

    ```tf
    resource "aws_db_instance" "kelly-tf-database" {
      identifier             = "kelly-tf-database" // this is the name of database
      allocated_storage      = 10
      db_name                = "postgres"
      engine                 = "postgres"
      engine_version         = "14.1"
      instance_class         = "db.t3.micro"
      username               = "autos"
      password               = "autos123"
      skip_final_snapshot    = true
      port                   = 5432
      vpc_security_group_ids = [aws_security_group.kelly-tf-db-security-group.id] // attaching aws_security_group
      publicly_accessible    = true
    }

    ```
- create Security Group for RDS to allow 5432 port

    ```tf
    resource "aws_security_group" "kelly-tf-db-security-group" {
      name        = "kelly-tf-db-security-group"
      description = "Allow Postgresql default port and SSH inbound"

      ingress {
        description = "Postgresql 5432 from anywhere"
        from_port   = 5432
        to_port     = 5432
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      }
      ingress {
        description = "SSH from anywhere"
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      }
      egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
      }

      tags = {
        Name = "kelly-tf-db-security-group" //this goes to Name section in console
      }
    }
    ```

- attach Security Group to RDS ("aws_db_instance")

    ```tf
    vpc_security_group_ids = [aws_security_group.kelly-tf-db-security-group.id]
    ```

- create DB table

    ```CREATE TABLE login (firstname VARCHAR(100), lastname VARCHAR(100));```

- get RDS endpoint and add it in **server.js** file
  1. when you run deploy RDS database, using **terraform output** to extract [db_address](https://gitlab.com/cicd-training1/kelly-project-database/-/blob/main/main.tf#L36).
  2. when [kelly-project-database pipeline](https://gitlab.com/cicd-training1/kelly-project-database/-/blob/main/.gitlab-ci.yml) is running, it's printing out the DB address. 
  3. grab this DB address, and replace **DB_HOST** in [kelly-project .gitlab-ci.yml DB_HOST variable](https://gitlab.com/cicd-training1/kelly-project/-/blob/main/.gitlab-ci.yml#L24).
  4. this variable is exported to terraform ```export TF_VAR_RDS_DB_HOST=$DB_HOST``` (variable should be named TF_VAR_~) 
  5. in main.tf, you can define this variable (TF_VAR_**RDS_DB_HOST** => RDS_DB_HOST) 
      ```
      # This is from .gitlab-ci.yml "export TF_VAR_RDS_DB_HOST=$DB_HOST"
      variable "RDS_DB_HOST" {
        type = string
      }
      ```
  6. pass this variable with user_data
      ```
        user_data = templatefile("${path.module}/backend_userdata.sh.tpl", {
          DB_HOST = "${var.RDS_DB_HOST}"
        })
      ```

  7. ```export DB_HOST=${DB_HOST}``` in backend_userdata.sh.tpl
  8. **server.js** host is set up ```host: process.env.DB_HOST```, so when you run ```export DB_HOST=${DB_HOST}```, it will set up it up the RDS DB_HOST.

<br>

### Frontend/Backend main.tf
- create S3 Bucket for Backend Code
    ```tf
    resource "aws_s3_bucket" "kelly-tf-backend" {
      bucket = "kelly-tf-backend"

      tags = {
        Name        = "kelly-tf-backend-tag"
        Environment = "Dev"
      }
    }
    ```

- create S3 Object to copy backend code
    ```tf
    resource "aws_s3_object" "backend_code" {
      bucket   = aws_s3_bucket.kelly-tf-backend.id
      for_each = setsubtract(fileset("./backend/", "**"), fileset("./backend/node_modules", "**"))
      key      = each.value
      source   = "./backend/${each.value}"
      etag     = filemd5("./backend/${each.value}")
    }
    ```
- create S3 Bucket for frontend Code
    ```tf
    resource "aws_s3_bucket" "kelly-tf-frontend" {
      bucket = "kelly-tf-frontend"

      tags = {
        Name        = "kelly-tf-frontend-tag"
        Environment = "Dev"
      }
    }
    ```
- create S3 Object to copy frontend code
    ```tf
    resource "aws_s3_object" "frontend_code" {
      bucket   = aws_s3_bucket.kelly-tf-frontend.id
      for_each = setsubtract(fileset("./frontend/", "**"), fileset("./frontend/node_modules", "**"))
      key      = each.value
      source   = "./frontend/${each.value}"
      etag     = filemd5("./frontend/${each.value}")
    }

    ```
- create Backend EC2 (Ubntu AMI)
    ```tf
    resource "aws_instance" "kelly-tf-backend" {
      ami                  = "ami-0557a15b87f6559cf"
      instance_type        = "t2.micro"
      key_name             = "kelly-keypair" // this is my kelly-keypair.pem file
      security_groups      = [aws_security_group.kelly-tf-backend-sg.name]
      iam_instance_profile = aws_iam_instance_profile.kelly-tf-backend-profile.id
      tags = {
        Name = "kelly-tf-backend"
      }

      user_data = templatefile("${path.module}/backend_userdata.sh.tpl", {
        DB_HOST = "${var.RDS_DB_HOST}"
      })

    }
    ```
- create Security Group for Backend EC2 to allow 4000 port
    ```tf

    resource "aws_security_group" "kelly-tf-backend-sg" {
      name        = "kelly-tf-backend-sg"
      description = "Allow 80 default port and SSH inbound"

      ingress {
        description = "HTTP 4000 from anywhere"
        from_port   = 4000
        to_port     = 4000
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      }
      ingress {
        description = "SSH from anywhere"
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]

      }
      egress {
        from_port        = 0
        to_port          = 0
        protocol         = "-1"
        cidr_blocks      = ["0.0.0.0/0"]
        ipv6_cidr_blocks = ["::/0"]
      }

      tags = {
        Name = "kelly-tf-backend-sg" //this goes to Name section in console
      }
    }
    ```
- attach Security Group to Backend EC2
    ```tf
      security_groups      = [aws_security_group.kelly-tf-backend-sg.name]
    ```
- create IAM Role for Backend EC2
    ```tf
    resource "aws_iam_role" "kelly-tf-backend-role" {
      name = "kelly-tf-backend-role"

      assume_role_policy = jsonencode({
        Version = "2012-10-17"
        Statement = [
          {
            Action = "sts:AssumeRole"
            Effect = "Allow"
            Sid    = ""
            Principal = {
              Service = "ec2.amazonaws.com"
            }
          },
        ]
      })

      tags = {
        tag-key = "tag-kelly"
      }
    }
    ```
- create In line policy for IAM Role to allow get code from S3 Bucket
    ```tf
    resource "aws_iam_role_policy" "kelly-tf-backend-role-policy" {
      name = "kelly-tf-backend-role-policy"
      role = aws_iam_role.kelly-tf-backend-role.id

      policy = jsonencode({
        Version = "2012-10-17"
        Statement = [
          {
            "Effect" : "Allow",
            "Action" : [
              "s3:ListBucket",
              "s3:GetObject"
            ],
            "Resource" : [
              "arn:aws:s3:::${aws_s3_bucket.kelly-tf-backend.id}",
              "arn:aws:s3:::${aws_s3_bucket.kelly-tf-backend.id}/*"
            ]
          },
          {
            "Effect" : "Allow",
            "Action" : "s3:ListAllMyBuckets",
            "Resource" : "*"
          },
        ]
      })
    }
    ```

- Create Instance Profile for backend EC2 to attach role
    ```tf
    resource "aws_iam_instance_profile" "kelly-tf-backend-profile" {
      name = "kelly-tf-backend-profile"
      role = aws_iam_role.kelly-tf-backend-role.name
    }
    ```
- attach Role to Backend EC2
    ```tf
    iam_instance_profile = aws_iam_instance_profile.kelly-tf-backend-profile.id
    ```
- create Frontend EC2
    ```tf
    resource "aws_instance" "kelly-tf-frontend" {
      ami                  = "ami-0557a15b87f6559cf"
      instance_type        = "t2.micro"
      key_name             = "kelly-keypair" // this is my kelly-keypair.pem file
      security_groups      = [aws_security_group.kelly-tf-frontend-sg.name]
      iam_instance_profile = aws_iam_instance_profile.kelly-tf-frontend-profile.id
      tags = {
        Name = "kelly-tf-frontend"
      }

      user_data = templatefile("${path.module}/frontend_userdata.sh.tpl", {
        backend_ip = aws_instance.kelly-tf-backend.public_ip
      })

    }
    ```
- create Security Group for Frontend EC2 to allow 80 port
    ```tf
    resource "aws_security_group" "kelly-tf-frontend-sg" {
      name        = "kelly-tf-frontend-sg"
      description = "Allow 80 default port and SSH inbound"

      ingress {
        description = "HTTP 80 from anywhere"
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      }
      ingress {
        description = "SSH from anywhere"
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]

      }
      egress {
        from_port        = 0
        to_port          = 0
        protocol         = "-1"
        cidr_blocks      = ["0.0.0.0/0"]
        ipv6_cidr_blocks = ["::/0"]
      }

      tags = {
        Name = "kelly-tf-frontend-sg" //this goes to Name section in console
      }
    }
    ```
- attach Security Group to Frontend EC2
    ```tf
    security_groups      = [aws_security_group.kelly-tf-frontend-sg.name]
    ```
- create IAM Role for Frontend EC2
    ```tf
    resource "aws_iam_role" "kelly-tf-frontend-role" {
      name = "kelly-tf-frontend-role"

      assume_role_policy = jsonencode({
        Version = "2012-10-17"
        Statement = [
          {
            Action = "sts:AssumeRole"
            Effect = "Allow"
            Sid    = ""
            Principal = {
              Service = "ec2.amazonaws.com"
            }
          },
        ]
      })

      tags = {
        tag-key = "tag-kelly"
      }
    }
    ```
- create In line policy for IAM Role to allow get code from S3 Bucket
    ```tf
    resource "aws_iam_role_policy" "kelly-tf-frontend-role-policy" {
      name = "kelly-tf-frontend-role-policy"
      role = aws_iam_role.kelly-tf-frontend-role.id

      policy = jsonencode({
        Version = "2012-10-17"
        Statement = [
          {
            "Effect" : "Allow",
            "Action" : [
              "s3:ListBucket",
              "s3:GetObject"
            ],
            "Resource" : [
              "arn:aws:s3:::${aws_s3_bucket.kelly-tf-frontend.id}",
              "arn:aws:s3:::${aws_s3_bucket.kelly-tf-frontend.id}/*"
            ]
          },
          {
            "Effect" : "Allow",
            "Action" : "s3:ListAllMyBuckets",
            "Resource" : "*"
          },
        ]
      })
    }
    ```
- create Instance Profile for Frontend EC2 to attach role
    ```tf
    resource "aws_iam_instance_profile" "kelly-tf-frontend-profile" {
      name = "kelly-tf-frontend-profile"
      role = aws_iam_role.kelly-tf-frontend-role.name
    }
    ```
- attach Role to Frontend EC2
    ```tf
    iam_instance_profile = aws_iam_instance_profile.kelly-tf-frontend-profile.id
    ```

