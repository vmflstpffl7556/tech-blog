+++
title = "Manual Deployement"
weight = 3
draft = false
+++


### Frontend(Manual Deployement with AWS Console)
- create S3 bucket to save frontend code
- copy frontend code to S3 bucket
- create EC2: ami-0557a15b87f6559cf (ubuntu)
- create Security Group that allows ingress 80(http) and 22(SSH from anywhere) 
- attach Security Group to EC2
- create IAM Role(List/Get S3 Bucket Where the frontend code is saved)
- attach IAM Role to EC2
- ```cd /home/vacpau``` (where keypair.pem is located)
- ```ssh -i "kelly-keypair.pem" ubuntu@ec2-100-26-56-138.compute-1.amazonaws.com```
  - ```sudo apt-get install curl```
  - ```curl -sL https://deb.nodesource.com/setup_16.x | sudo -E bash -```
  - ```sudo apt-get install nodejs ```  
  - ```sudo apt install awscli -y```
  - ```sudo aws s3 cp s3://kelly-tf-frontend/ /home/ubuntu/kelly-frontend --recursive``` : copy frontend code from S3 bucket to EC2 instance 
  - ```cd /home/ubuntu/kelly-frontend```
  - ```sudo rm -rf dist```
  - ```sudo npm i @vue/cli```
  - ```sudo npm i @vue/cli-service```
  - ```sudo npm run build```
  - ```sudo apt-get install nginx -y```
  - ```sudo cp /home/ubuntu/kelly-frontend/dist/* /var/www/html --recursive```
- Optional Check
  - ```sudo nginx -t```
  - ```sudo service nginx reload```
- Note: Different way to deploy frontend: run this command 
    - ```sudo aws s3 cp s3://kelly-frontend/ /usr/share/nginx/html --recursive```
    - need to ```cd /etc/nginx/sites/available``` and modify default file (root)
         ```
        server {
                listen 80;
                listen [::]:80;

                server_name 127.0.0.1;

                root /usr/share/nginx/html/dist;
                index index.html;

                location / {
                        root /usr/share/nginx/html/dist;
                        try_files $uri /index.html;
                }
        }
        ```
<br>

### Backend(Manual Deployement with AWS Console)
- create S3 bucket to save backend code
- copy backend code to S3 bucket
- create EC2: ami-0557a15b87f6559cf (ubuntu) 
- create Security Group that allows ingress 4000 and 22(SSH from anywhere) 
- attach Security Group to EC2
- create IAM Role(List/Get S3 Bucket Where the backend code is saved)
- attach IAM Role to EC2
- ```cd /home/vacpau``` (where keypair.pem is located)
- ```ssh -i "kelly-keypair.pem" ubuntu@ec2-100-26-56-138.compute-1.amazonaws.com```
  - ``sudo apt-get install curl``
  - ``curl -sL https://deb.nodesource.com/setup_16.x | sudo -E bash -``
  - ``sudo apt-get install nodejs  ``   
  - ``sudo apt install awscli -y``
  - ``sudo aws s3 cp s3://kelly-backend/ /home/ubuntu/kelly-backend --recursive``
  - ``cd /home/ubuntu/kelly-backend``
  - ``sudo npm install``
  - ``sudo npm install -g pm2``
  - ``cd /home/ubuntu/kelly-backend/src``
  - ``pm2 start server.js``
- Test
  - ec2-52-90-12-250.compute-1.amazonaws.com:4000/api/list

<br>

### Databse(Manual Deployement with AWS Console)
- create RDS
    - user: 'autos', // DB User
    - password: 'autos123', // DB Password
    - database: 'postgres',
- After deploy, check the connection
  - ```psql --host=kelly-tf-database.ckdatbejuaih.us-east-1.rds.amazonaws.com --port=5432 --username=autos --password --dbname=postgres```
- Create table 
  - ```CREATE TABLE login (firstname VARCHAR(80), lastname VARCHAR(80));```
- ```\d``` : to list the table

