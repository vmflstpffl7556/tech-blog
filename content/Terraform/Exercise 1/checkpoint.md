+++
title = "Checkpoint"
weight = 1
draft = false
+++


1. Create a new project

2. Initialize the project as a git repository: ```git init```.

3. Create a **.gitignore** file with these contents [.gitignore - Terraform template](https://github.com/github/gitignore/blob/main/Terraform.gitignore)

4. Create a **/build/libs** directory in your project and copy over your Spring Boot application archive file (.jar). You may need to build the project to produce a .jar file.

5. Create a **main.tf** file and paste in these contents to get started - make a note to set your region as intended

      ```
        terraform {
          required_providers {
              aws = {
              source  = "hashicorp/aws"
              version = "~> 4.0"
              }
          }
        }

        provider "aws" {
        region = "us-east-1"
        }
      ```

6. Initialize the Terraform project with **terraform init**

7. Begin defining your resources.

    - Work slowly, one resource at a time, and verify your configuration with terraform plan. Apply it and check the AWS management console that your resources are provisioned properly.
    - Running terraform apply is relatively cheap in terms of time. It can be easy to fall into the habit of trial and error so try to slow down and be deliberate about the changes you make.

8. It is now up to you to define the stack. You will need, at minimum, the following resources. Do not attempt to define it all at once before testing. Much of working with Terraform is searching provider documentation. **The order in which you create these resources is not required (although creating them in this order does simplify things). You may choose your own order to create these resources. Be sure to note all console outputs and errors as your work through the list.**

    - aws_s3_bucket
    - aws_s3_object
    - aws_security_group
      - aws_default_vpc (optional, see aws_security_group hint for details)
    - aws_iam_role_policy
    - aws_iam_role
    - aws_iam_instance_profile
    - aws_instance

9. **aws_instance** provisioning an EC2 instance with one command ``terraform apply`` is powerful and convenient! But what about all the software installation and shell commands that we typically run - commands like ```aws s3api get-object```? There is a very helpful feature in EC2 called **User Data** where one can supply scripts that will run once the EC2 instance launches. When launching a new instance, there will be an Advanced Details section. At the bottom of this section, you can input a launch user data script.

    - Look into creating this script (be sure to refer back to all the scripts we ran in the EC2 deployment unit) and see the Hints section below for a working set of scripts.
    - Please see [AWS Docs - User Data Scripts](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/user-data.html) for information on how to debug broken scripts

10. When complete, you should be able to deploy and destroy the entire stack with ```terraform apply```, see your application live at the EC2 instance's public IP/DNS address, and ```terraform destroy``` to remove all resources from AWS.