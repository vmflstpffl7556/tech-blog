+++
title = "Exercise 1"
date = 2022-09-20T09:45:34-04:00
weight = 5
chapter = true
pre = "<b></b>"
+++

## Terraform with AWS


##### Project Repository: [Zoo Tdd Spring](https://gitlab.com/cicd-training1/zoo-tdd-spring)
##### Project Description: Deploying Spring Boot application in AWS EC2 with Terraform

### Index
{{% children %}}
