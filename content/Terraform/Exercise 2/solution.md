+++
title = "Solution"
weight = 3
draft = false
+++


1. Create a new g-autos project(Spring+Postgresql)

2. Initialize the project as a git repository: **git init**

3. Create a **.gitignore** file with these contents [.gitignore - Terraform template](https://github.com/github/gitignore/blob/main/Terraform.gitignore)

4. run ```./gradlew build```: it will create jar file in `build/libs`

5. Create a **main.tf** file and paste in these contents to get started - make a note to set your region as intended
    - backend "http": Previously, we used a singular local Terraform state file. Using CICD, this state file, while not to be tracked in the repository, will still need to be accessed by runners that are performing Terraform operations. 

      ```
        terraform {
          required_providers {
              aws = {
              source  = "hashicorp/aws"
              version = "~> 4.0"
              }
          }
        }

        provider "aws" {
        region = "us-east-1"
        }
      ```

6. Initialize the Terraform project with ```terraform init```

7. create **AWS S3 Bucket**

      ```
      resource "aws_s3_bucket" "kelly-s3-bucket-autos" {
        bucket = "kelly-tf-s3-bucket-autos" // name of the bucket

        tags = {
          Name        = "My bucket"
          Environment = "Dev"
        }
      }
      ```


8. create **aws_s3_object**

      ```
      resource "aws_s3_object" "kelly-s3-object-autos" {
        bucket = aws_s3_bucket.kelly-s3-bucket-autos.id // kelly-s3-bucket is referring to aws_s3_bucket that I created above
        key    = "g-autos-0.0.1-SNAPSHOT"  // Name of the object
        source = "./build/libs/g-autos-0.0.1-SNAPSHOT.jar" //the location in local
        source_hash = filemd5("build/libs/g-autos-0.0.1-SNAPSHOT.jar")
      }
      ```

9.  create Spring Boot EC2 **aws_instance**
    - ami AMIs are specific to regions. The Amazon Linux 2 AMI has different IDs depending on the region. Use one of the following methods to source this ID.
    - ```key_name``` - Create a key pair in the AWS Management Console at the EC2 dashboard or reference a key you have already created. This argument creates the EC2 instance with the capability of connecting to it via SSH and authenticating with your private key. If you do NOT include this argument, the instance will still be created but you will have no way to administer the instance by connecting to it. Pairs switching after this value is set will need to provide an updated key name. 

      ```
      resource "aws_instance" "kelly-tf-ec2-instance-autos" {
        ami           = "ami-0dfcb1ef8550277af"
        instance_type = "t2.micro"
        key_name      = "kelly-keypair" // this is my kelly-keypair.pem file
        tags = {
          Name = "kelly-tf-ec2-instance-autos"
        }
        user_data = <<EOF
          #! /bin/bash
          sudo yum update -y
          sudo yum install -y java-11-amazon-corretto-headless
          aws s3api get-object --bucket "${aws_s3_bucket.kelly-s3-bucket-autos.id}" --key "${aws_s3_object.kelly-s3-object-autos.key}" "${aws_s3_object.kelly-s3-object-autos.key}"
          java -jar ${aws_s3_object.kelly-s3-object-autos.key}
          EOF
      }
      ```

10. create **aws_security_group** for EC2: to allow 8080 and 22 port

      ```
      resource "aws_security_group" "kelly-security-group-autos" {
        name        = "kelly-tf-security-group-autos"
        description = "Allow Spring default port and SSH inbound"

        ingress {
          description = "HTTP 8080 from anywhere"
          from_port   = 8080
          to_port     = 8080
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
        }
        ingress {
          description = "SSH from anywhere"
          from_port   = 22
          to_port     = 22
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
        }
        egress {
          from_port        = 0
          to_port          = 0
          protocol         = "-1"
          cidr_blocks      = ["0.0.0.0/0"]
        }

        tags = {
          Name = "kelly-tf-security-group-autos-tag" //this goes to Name section in console
        }
      }
      ```

11. Attaching **Security Group** to **EC2 Instance**
    - Attach the security group that you just created which allows 8080 and 22 inbound rules to EC2 Instance
    ```security_groups = [ aws_security_group.kelly-security-group-autos.id ]``` adding in resource "aws_instance" "kelly-security-group-autos"


12. create **am_iam_role**

      ```
      resource "aws_iam_role" "kelly-tf-role-autos" {
        name = "kelly-tf-role-autos-list-get-s3"

        assume_role_policy = jsonencode({
          Version = "2012-10-17"
          Statement = [
            {
              Action = "sts:AssumeRole"
              Effect = "Allow"
              Sid    = ""
              Principal = {
                Service = "ec2.amazonaws.com"
              }
            },
          ]
        })

        tags = {
          tag-key = "tag-kelly"
        }
      }
      ```


13. create **aws_iam_policy**
    - aws_iam_policy: creating policy
    - aws_iam_role_policy: This resource creates an IAM Role Policy that can be **defined inline**.

      ```
      resource "aws_iam_policy" "kelly-tf-iam-policy-autos" {
        name        = "kelly-tf-iam-policy-autos"
        description = "s3 bucket list get policy"
        policy = jsonencode(
          {
            "Version" : "2012-10-17",
            "Statement" : [
              {
                "Effect" : "Allow",
                "Action" : [
                  "s3:ListBucket",
                  "s3:GetObject"
                ],
                "Resource" : [
                  "arn:aws:s3:::${aws_s3_bucket.kelly-s3-bucket-autos.id}",
                  "arn:aws:s3:::${aws_s3_bucket.kelly-s3-bucket-autos.id}/*"
                ]
              },
              {
                "Effect" : "Allow",
                "Action" : "s3:ListAllMyBuckets",
                "Resource" : "*"
              }
            ]
          }
        )
      }
      ```

14. Attaching **Policy** to **IAM Role**
      ```
      resource "aws_iam_role_policy_attachment" "kelly-policy" {
        role       = aws_iam_role.kelly-tf-role-autos.id
        policy_arn = aws_iam_policy.kelly-tf-iam-policy-autos.id
      }
      ```

15. create **aws_iam_instance_profile**
    - Attaching **IAM Role** to **EC2 Instance** (so ec2 instance have a permission to get/list s3 bucket)

      ```
      resource "aws_iam_instance_profile" "kelly-tf-iam-instance-profile" {
        name = "kelly-tf-iam-instance-profile-autos"
        role = aws_iam_role.kelly-tf-role-autos.name
      }
      ```

16. Attaching **aws_iam_instance_profile** to **EC2 instance**
    
    ```iam_instance_profile = aws_iam_instance_profile.kelly-tf-iam-instance-profile.id``` in resource "aws_instance" "kelly-tf-ec2-instance-autos"

17. create **aws_db_instance** : RDS Postgresql
  - I was not able to connect to RDS, and after adding ```  publicly_accessible = true```, it worked.
  - You can check if you can connect from your local with ```psql --host=kelly-tf-db-autos.ckdatbejuaih.us-east-1.rds.amazonaws.com --port=5432 --username=autos --password --dbname=postgres```

        ```
        resource "aws_db_instance" "kelly-tf-db-autos" {
          identifier          = "kelly-tf-db-autos" // this is the name of database
          allocated_storage   = 10
          db_name             = "postgres" //This has to match from application.properties
          engine              = "postgres"
          engine_version      = "14.1"
          instance_class      = "db.t3.micro"
          username            = "autos" //This has to match from application.properties
          password            = "autos123" //This has to match from application.properties
          skip_final_snapshot = true
          port                = 5432
          publicly_accessible = true
        }
        ```

18. create **aws_security_group** for Postgresql RDS
      ```
      resource "aws_security_group" "kelly-security-group-autos2" {
        name        = "kelly-tf-security-group-autos2"
        description = "Allow Postgresql default port and SSH inbound"

        ingress {
          description = "Postgresql 5432 from anywhere"
          from_port   = 5432
          to_port     = 5432
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
        }
        ingress {
          description = "SSH from anywhere"
          from_port   = 22
          to_port     = 22
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
        }
        egress {
          from_port   = 0
          to_port     = 0
          protocol    = "-1"
          cidr_blocks = ["0.0.0.0/0"]
        }

        tags = {
          Name = "kelly-tf-security-group-autos2-tag" //this goes to Name section in console
        }
      }
      ```

19. Attaching **security group** to **RDS**
    ```  vpc_security_group_ids = [aws_security_group.kelly-security-group-autos2.id]``` in 
    resource "aws_db_instance" "kelly-tf-db-autos"

21. Adding RDS DB_HOST in user_date

    ```
      user_data = <<EOF
          #! /bin/bash
          sudo yum update -y
          sudo yum install -y java-11-amazon-corretto-headless
          export DB_HOST="${aws_db_instance.kelly-tf-db-autos.address}" 
          aws s3api get-object --bucket "${aws_s3_bucket.kelly-s3-bucket-autos.id}" --key "${aws_s3_object.kelly-s3-object-autos.key}" "${aws_s3_object.kelly-s3-object-autos.key}"
          echo "Here is DB_HOST"
          echo $DB_HOST
          java -jar ${aws_s3_object.kelly-s3-object-autos.key}
          EOF
    ```

22. Test if you can connect to RDS
   - run this from local
      ```psql --host=kelly-tf-db-autos.ckdatbejuaih.us-east-1.rds.amazonaws.com --port=5432 --username=autos --password --dbname=postgres```
   - ssh to EC2
     ```ssh -i "kelly-keypair.pem" ec2-user@ec2-100-26-153-162.compute-1.amazonaws.com```
   - check the log
     - cd /var/log
     - sudo cat cloud-init-output.log

23. Testing

    - ```http://EC2_Public_IPv4_DNS:8080/``` (ex: http://ec2-34-205-171-79.compute-1.amazonaws.com:8080/)  => whitelabel Error
    - ```http://EC2_Public_IPv4_DNS:8080/autos```

24. push this repository to gitlab

25. add backend to use S3 bucket in main.tf
   - Creating S3 Bucket to save terraform.state file
   - reference: https://www.youtube.com/watch?v=RBW253A4SvY
      ```
        backend "s3" {
          bucket = "kelly-s3-bucket-for-tf-state"
          key = "tf-g-autos/terraform.tfstate"
          region = "us-east-1"
        }
      ```

26. add AWS Configure variables in Settings/cicd/variables
   - AWS_ACCESS_KEY_ID
   - AWS_DEFAULT_REGION 
   - AWS_SECRET_ACCESS_KEY

