+++
title = "Exercise 2"
date = 2022-09-20T09:45:34-04:00
weight = 5
chapter = true
pre = "<b></b>"
+++

## Terraform with AWS

##### Project Repository: [TF G Autos](https://gitlab.com/cicd-training1/tf-g-autos)
##### Project Description: Deploying Spring Boot application in AWS EC2 and PostgreSQL in AWS RDS with Terraform


### Index
{{% children %}}
