+++
title = "Terraform.tfstate"
weight = 2
draft = false
+++

#### Option 1. GitLab-managed Terraform state
- Using CICD, this state file, while not to be tracked in the repository, will still need to be accessed by runners that are performing Terraform operations. Remote backends for state storage are quite common and GitLab has an integrated storage solution for Terraform state files we can leverage.

  - Prepare your project to use Gitlab's Terraform state backend. Modify your main.tf file's terraform block to look like this:

    ```
    terraform {
        required_providers {
            aws = {
            source  = "hashicorp/aws"
            version = "~> 4.0"
            }
        }
        backend "http" {
        }
    }
    ```

#### Option 2. AWS S3 Bucket Terraform state
- You can save terraform.state file in S3 bucket, so you can run ```terraform apply``` in pipeline and local.
- Create S3 bucket in AWS name "kelly-s3-bucket-for-tf-state"
- add backend "s3" in main.tf
    ```
    terraform {
        required_providers {
            aws = {
            source  = "hashicorp/aws"
            version = "~> 4.0"
            }
        }
        backend "s3" {
            bucket = "kelly-s3-bucket-for-tf-state"
            key    = "tf-kelly-project/terraform.tfstate"
            region = "us-east-1"
        }
    }
    ```
- go to S3 bucket, and there is a terraform.tfstate file inside of tf-kelly-project folder