+++
title = "Terraform Tips"
weight = 3
draft = false
+++

#### terraform CLI
- ```terraform apply -replace="aws_instance.myinstancename"``` => this will only update one.(destroy ec2 and restart)
- ex: ```terraform apply -replace="aws_instance.kelly-tf-ec2-instance"```
- ``aws iam delete-instance-profile --instance-profile-name ExampleInstanceProfile`` : you can only delete instance profile with AWS CLI. You cannot delete via AWS Console.
#### troubleshooting
- ```cd /var/log```
- ```cloud-init-output.log``` file

