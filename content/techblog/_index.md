+++
title = "Tech Blog"
weight = 1
draft = false
+++
<!--when the draft = true => it wont' show on the website 
weight will decide the order of menu (the lower the first )-->

![Profile](../images/profilepic.jpg?width=550px&height=350px)


Hello! My name is Kelly Lee (My Korean name is Yanghee Lee) and I am a software engineer currently living in Dunwoody, GA. 

I am originally from South Korea. I studied Material Science Engineering at Inha Univercity in South Korea, where I had the opportunity to join a study abroad program at Illionois Institute of Technology in Chicago which was my first time to come to United States in 2013. I studied for one semester and had an internship for 2 months, and went back to South Korea to finish my bachelor's degree. However, living in the US for 6 months was amazing, and I wanted to come back, so I got another internship during my senior year and came to Atlanta, GA. I was offered a job as a project coordinator in a German engineering company, and my goal for this internship was learning English and getting more experience, and going back to Korea! However, one year of internship still felt too short, and fortunately the company that I worked for offered me a full time job after my internship ended. So I decided to stay in Georgia, and also started my MBA. I was working full time, and studied in night school to complete my masters in 2018. I continued working for INP North America, where I learned a great deal by working for several on-site projects. INP North America was later merged with Actemium, and I was transfered to Germany where I worked as a Project Manager during the construction of a BASF chemical plant. 

Ultimately my desire to fully understand each component of my projects led me to pursue additional training and development in the engineering sector. Therefore, I decided to resign from my job in Germany and went back to South Korea to learn software development. I completed a software engineering bootcamp covering full-stack development, and subsequently worked for a software company that provides costing outsourcing service to major hospitals in South Korea. Throughout this time, I gained hands-on experience coding under the direction of a team, collaborating on software projects, and testing systems and applications. One of my responsibilities was developing the company’s internal website to upgrade the company’s own solution programs using Spring Boot, PostgreSQL, and AWS. These experiences allowed me to develop my skills in learning new materials quickly, thinking on my feet, and adapting to an entirely new industry.

In 2020 I got married and moved to the US permanently, and I'm now working as a software engineer at State Farm. My primary responsibilities are developing, maintaning, and deploying our internal IAM application using CI/CD, GitOps, Kubernes, Docker, Helm, etc.

I am very passionate about learning new things, have a diligently positive outlook on life, and I am always looking forward to what will come next in my career and my life overall. 