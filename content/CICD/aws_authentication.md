+++
title = "AWS Authentication in GitLab"
weight = 1
draft = false
+++

## AWS Authentication
- GitLab runners do not have access to our AWS accounts. Therefore, you need to set up AWS credentials in Gitlab project to access AWS.

- How do I authenticate to AWS for my pipeline?
  - Project > Settings > CI/CD > Variables
    - ```AWS_ACCESS_KEY_ID```: Your Access key ID
    - ```AWS_SECRET_ACCESS_KEY```:	Your Secret access key
    - ```AWS_DEFAULT_REGION```:	Your region code
  - You can also set it up as a Group level

