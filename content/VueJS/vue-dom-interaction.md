+++
title = "Vue DOM Interaction"
weight = 3
draft = false
+++


#### v-bind
- v-bind: binding attributes
    ```html
    <a href="{{ vueLink }}"> //this does not work
    <a v-bind:href="vueLink"> // this works 
    ```
    ```js
        data(){ //always return the object
            return {
                vueLink: 'https://vuejs.org/'
            }; 
        } 
    ``` 

#### methods
- how to call method
    ```html
    <p>{{ outputGoal() }}</p>
    ```
    ```js
        methods:{
                outputGoal(){
                    const randomNumber = Math.random();
                    if(randomNumber<0.5){
                        return 'Learn Vue!';
                    }else{
                        return 'Master Vue!';
                    }
                }
            }
    ```
    

#### v-html
```html
<p v-html="courseGoal"></p>
```
```js
    data(){
        return {
            courseGoal: '<h2>Master Vue and build amazing apps</h2>',
        }; 
    },
```
