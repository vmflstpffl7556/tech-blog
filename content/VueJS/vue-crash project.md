+++
title = "Vue Crash Project"
weight = 7
draft = false
+++

### Vue-Crash-2022 Project

1. create vue-crash-2022 project
    - `vue create vue-crash-2022`
    - Manually Select
        - Babel
    - Vuee 3
    - dedicated config file
2. main.js : entry point for vue
3. delete HelloWorld component from App.vue
4. add style in App.vue
5. create Header.vue
    - add style
6. import Header in App.vue / add in components
7. create Button.vue
  - create button in template
  - created script button  
  - create onclick method
8. import Button in Header.vue
9. add data() function in App.vue
10. add created() function and add some task datas in App.vue
11. create Tasks.vue
12. 
