+++
title = "Vue Project Creation"
weight = 2
draft = false
+++
<!--when the draft = true => it wont' show on the website 
weight will decide the order of menu (the lower the first )-->

### How to create Vue project with vue-cli

 - Creating Vue Project
   - `vue create project`
   - selection: 
![Profile](../../images/create-vue-project.png?width=850px&height=150px)
 - Project Structure
    ```
    Project01
    ├── node_modules // 설치된 node모듈이 위치해 있는 폴더(npm install 명령어를 통해 설치한 모듈이 위치)
    ├── public // index.html파일이 위치하는 곳(정적 파일이 위치하는 곳)
    ├── src // 구현되는 vue 컴포넌트 파일이 위치하는 곳
    │   ├── assets // css, image등 파일이 위치하는 곳
    │   ├── components // Vue 컴포넌트중에 재사용을 위해서 구현된 컴포넌트가 위치하는 곳
    │   ├── router // 라우팅을 정의하는 파일이 위치하는 곳
    │   ├── store // vuex의 상태저장소인 store파일이 위치하는 곳
    │   ├── views // 웹 애플리케이션에서 각 화면, 즉 메뉴에 대응되는 화면에 해당하는 Vue 컴포넌트가 위치하는 곳
    │   ├── App.vue // 최상위 Vue 컴포넌트
    │   ├── main.js
    ├── babel.config.js
    ├── jsconf.json
    ├── package-lock.json
    ├── package.json // Vue 프로젝트에 대한 정보 및 사용하고 있는 모듈 등에 대한 정보를 관리하고, Vue 프로젝트를 실행 할 수 있는 스크립트가 정의된 파일
    └── vue.config.js   
    ```
 - Understand Project Start Process
   1. start Vue Project `npm run serve`
   2. main.js
   3. main.js load router, vuex, mixins 
   4. App.vue
   5. mount on public/index.html


- App.vue
  ```js
  <template>
    <nav>
      //router-link to is looking for the same path in router/index.js path, and use that component
      <router-link to="/">Home</router-link> | //This to="/" has to match to path in router/index.js path
      <router-link to="/about">About</router-link>
    </nav>
    //anything below router-view is changing(dynamic)
    <router-view/>
  </template>
  <style>
  </style>


- router/index.js
   ```js
    import { createRouter, createWebHistory } from 'vue-router'
    import HomeView from '../views/HomeView.vue'

    const routes = [
      {
        path: '/',
        name: 'home',
        component: HomeView // from '../views/HomeView.vue'
      },
      {
        path: '/about',
        name: 'about',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
      }
    ]

    const router = createRouter({
      history: createWebHistory(process.env.BASE_URL),
      routes
    })

    export default router


- src/views/HomeView.vue
  - **@** is an alias to /src folder path
  ```js
  <template>
    <div class="home">
      <img alt="Vue logo" src="../assets/logo.png">
      // it's using HelloWorld
      <HelloWorld msg="Welcome to Your Vue.js App"/>
    </div>
  </template>

  <script>
  // @ is an alias to /src
  import HelloWorld from '@/components/HelloWorld.vue'

  export default {
    name: 'HomeView',
    components: {
      HelloWorld
    }
  }
  </script>

 - App.vue(router-link to) => router/index.js (same path exists) => component:HomeView (using HomeView.vue)

 