+++
title = "Vue Basic"
weight = 1
draft = false
+++
<!--when the draft = true => it wont' show on the website 
weight will decide the order of menu (the lower the first )-->

### Basic Vue.js

- How to check if you already have vue installed
  - `vue --version` : checking vue version

- How to install vue
  - `npm install -g vue` 
    - g: install globally. 
  - `vue --version`

- How to install Vue CLI
  - `npm install -g @vue/cli`
  - vue cli is a tool **to create vue project quickly and easily**

- Install VSCode extendsion: **Vetur**

- Create Vue Project
  - `vue create project01`
    - select Default([Vue 3] babel, eslint)

  - This will create default files and folders
    ```
    Project01
    ├── node_modules
    ├── public
    ├── src
    │   ├── assets
    │   ├── components
    │   ├── App.vue
    │   ├── main.js
    ├── babel.config.js
    ├── jsconf.json
    ├── package-lock.json
    ├── package.json
    └── vue.config.js   
    ```
- How to start the project
  - `cd project01`
  - `npm run serve`
  - you can go to http://localhost:8080/
- package.json
    ```json 
    {
      "name": "project01",
      "version": "0.1.0",
      "private": true, //default is True
      "scripts": { 
        "serve": "vue-cli-service serve", //npm run serve = npm run vue-cli-service serve
        "build": "vue-cli-service build",
        "lint": "vue-cli-service lint"
      },
      "dependencies": { //modules that you need to run the app
        "core-js": "^3.8.3",
        "vue": "^3.2.13"
      },
      "devDependencies": { //modules that you need to develop the app, each modules are written in package-lock.json
        "@babel/core": "^7.12.16",
        "@babel/eslint-parser": "^7.12.16",
        "@vue/cli-plugin-babel": "~5.0.0",
        "@vue/cli-plugin-eslint": "~5.0.0",
        "@vue/cli-service": "~5.0.0",
        "eslint": "^7.32.0",
        "eslint-plugin-vue": "^8.0.3"
      },
      "eslintConfig": {
        "root": true,
        "env": {
          "node": true
        },
        "extends": [
          "plugin:vue/vue3-essential",
          "eslint:recommended"
        ],
        "parserOptions": {
          "parser": "@babel/eslint-parser"
        },
        "rules": {}
      },
      "browserslist": [
        "> 1%",
        "last 2 versions",
        "not dead",
        "not ie 11" //does not support Internet explorer 11
      ]
    }



- main.js : when you run `npm run serve`, the very first file that it runs
    ```js
    import { createApp } from 'vue'
    import App from './App.vue'

    createApp(App).mount('#app')
    ```







