+++
title = "Vue Databinding"
weight = 4
draft = false
+++

### Vue Databinding

- views folder: 화면전체
  - naming rule: ~View.vue (component와 다른점)
    - HomeVue.vue
    - AboutVue.vue
- components folder: resuable(재사용가능한)

- create folder views/1_databinding

#### DataBinding String
1. create [DataBindingStringView.vue](https://github.com/kellylee-dev/Vue-study/blob/master/src/views/1_databinding/DataBindingStringView.vue): **문자열 바인딩**
    - 문자열 바인딩 **{{ message }}**
2. add in router/index.js file
    ```js
    {
      path: '/databinding/string',
      name: 'DataBindingStringView',
      component: () =>
        import(
          /* webpackChuncName: "databinding", webpackPrefetch:true */ '../views/1_databinding/DataBindingStringView.vue'
        )
    }
    ```
3. `npm run serve` localhost:8080/databinding/string

#### DataBinding HTML
1. create [DataBindingHtmlView.vue](https://github.com/kellylee-dev/Vue-study/blob/master/src/views/1_databinding/DataBindingHtmlView.vue): **HTML 바인딩**
   - HTML binding: **v-html** 사용
   - `` <div v-html="htmlString"></div>``
      ```js
        data () {
          return {
            htmlString: '<p style="color:red;">RED world</p>'
          }
        }
2. add in router/index.js file
    ```js
    {
      path: '/databinding/html',
      name: 'DataBindingHtmlView',
      component: () =>
        import(
          /* webpackChuncName: "databinding", webpackPrefetch:true */ '../views/1_databinding/DataBindingHtmlView.vue'
        )
    }  
    ```

#### DataBinding Input
  1. create [DataBindingInputView.vue](https://github.com/kellylee-dev/Vue-study/blob/master/src/views/1_databinding/DataBindingInputView.vue): **v-model**은 **양방향 데이터 바인딩**
      - **v-model**은 양방향 데이터 바인딩
      - onClick과 **@click**은 같다
      - **v-model.number**을 사용하면 자동으로 숫자로 바뀜. 문자가아닌 숫자로 인식됨
  2. add in router/index.js


#### DataBinding Select
1. create [DataBindingSelectView.vue](https://github.com/kellylee-dev/Vue-study/blob/master/src/views/1_databinding/DataBindingSelectView.vue)
   - v-model사용/양방향 데이터 바인딩
2. add in router/index.js


#### DataBinding CheckBox
1. create [DataBindingCheckboxView.vue](https://github.com/kellylee-dev/Vue-study/blob/master/src/views/1_databinding/DataBindingCheckboxView.vue)
    - 체크박스인 경우 v-model은 value값과 양방향 데이터 바인딩이 아니라 checked로 데이터바인딩이 된다(사용자가 value값을 변경하는 것이 아니라 checked값 true/false를 바꾼는 것이기때문에)
    - input이나 select은 value값과 양방향 데이터 바인딩이다
    - 모든 체크박스에 v-model에 같은 key값인 favoriteLang을 넣는다 => 그룹으로 묶인다(name대신 사용)
2. add in router/index.js

#### DataBinding Radio
1. create [DataBindingRadioView.vue](https://github.com/kellylee-dev/Vue-study/blob/master/src/views/1_databinding/DataBindingRadioView.vue)
   - radio는 한가지만 선택할수있다. 체크박스와 다른점. 따라서 배열이 아니라 문자열로선언해야한다
2. add in router/index.js

#### DataBinding Attribute
1. create [DataBindingAttributeView.vue](https://github.com/kellylee-dev/Vue-study/blob/master/src/views/1_databinding/DataBindingAttributeView.vue)
   - **readonly**일때 양방향으로 할필요가 없다. 단방향으로 사용: v-model대신 **v-bind**사용
   - v-bind:value 와 :value는 같다. v-bind생략가능
   - 조회버튼 v-model양방향사용/어떤 값이라도 들어와야 조회가 가능하게 만듬/값이 없을때는 조회버튼이 비활성화되있음
      ```html
        <input type="search" name="" id="" v-model="txt1">
        <button :disabled="txt1 === ''">search</button>``
      ```
2. add in router/index.js

#### DataBinding List
1. create [DataBindingListView.vue](https://github.com/kellylee-dev/Vue-study/blob/master/src/views/1_databinding/DataBindingListView.vue) 배열로 데이터 바인딩
   - v-for 사용. v-for사용시 :key를 반드시 잡아줘야 한다.(key값은 유일해야한다)
    ```html
      <option :value="city.code" :key="city.code" v-for="city in cities">{{ city.title }}</option>
    ```
2. add in router/index.js


#### DataBinding Class
1. create [DataBindingClassView.vue](https://github.com/kellylee-dev/Vue-study/blob/master/src/views/1_databinding/DataBindingClassView.vue) **클래스 바인딩**
   - **:class** 사용
2. add in router/index.js

#### DataBinding Style
1. create [DataBindingStyleView.vue](https://github.com/kellylee-dev/Vue-study/blob/master/src/views/1_databinding/DataBindingStyleView.vue) **Style 바인딩**
   - **:style** 사용
2. add in router/index.js